import { Component } from '@angular/core'
import { State } from '@ngrx/store'
import { AppState } from './redux/app.state'
import { filter } from 'rxjs/operators'
import {
  Router,
  // import as RouterEvent to avoid confusion with the DOM Event
  Event as RouterEvent,
  NavigationStart,
  NavigationEnd,
  NavigationCancel,
  NavigationError
} from '@angular/router'
import { Meta } from '@angular/platform-browser'
import { DefaultService } from './services/global/default.service'

declare var gtag

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'Unisinos'
  show = true

  constructor(private state: State<AppState>,
    private meta: Meta,
    private dService: DefaultService,
    private router: Router) {
    router.events.subscribe((event: RouterEvent) => {
      this.navigationInterceptor(event)
    })
    const navEndEvents = router.events.pipe(
      filter(
        event => event instanceof NavigationEnd
      )
    )

    navEndEvents.subscribe(
      (event: NavigationEnd) => {
        gtag('config', 'GTM-TMF8992', {
          page_path: event.urlAfterRedirects
        })
      }
    )
  }


  // Shows and hides the loading spinner during RouterEvent changes
  navigationInterceptor(event: RouterEvent): void {
    if (event instanceof NavigationStart) {
      this.show = true
    }
    if (event instanceof NavigationEnd) {
      this.show = false
    }

    // Set loading state to false in both of the below events to hide the spinner in case a request fails
    if (event instanceof NavigationCancel) {
      this.show = false
    }
    if (event instanceof NavigationError) {
      this.show = false
    }
  }
}


export enum statusPublication {
  Draft = 0,
  New = 1,
  Feedback = 2,
  FeedbackAnswered = 3,
  All = 4
}

export enum OrderSelect {
  Recent = 0,
  Old = 1,
  AlfaOrder = 2,
  AlfaInverseOorder = 3
}

export let Activity = {
  LOGIN: {
    VALID: {
      ID: 0,
      DESCRIPTION: 'Login válido'
    },
    INVALID: {
      ID: 1,
      DESCRIPTION: 'Login inválido'
    },
    LOGOUT: {
      ID: 2,
      DESCRIPTION: 'LOGOUT'
    }
  },
  USER: {
    ADD: {
      ID: 3,
      DESCRIPTION: 'Inclusão de usuário'
    },
    EDIT: {
      ID: 4,
      DESCRIPTION: 'Alteração de usuário'
    }
  },
  PORTFOLIO_CREATED: {
    ID: 5,
    DESCRIPTION: 'Criação de Porfólio'
  },
  PUBLICATION: {
    ADD: {
      ID: 6,
      DESCRIPTION: 'Inclusão de Publicação'
    },
    EDIT: {
      ID: 7,
      DESCRIPTION: 'Alteração de Publicação'
    },
    DELETE: {
      ID: 8,
      DESCRIPTION: 'Exclusão de Publicação'
    }
  },
  FEEDBACK: {
    ADD: {
      ID: 9,
      DESCRIPTION: 'Inclusão de Feedback'
    },
    EDIT: {
      ID: 10,
      DESCRIPTION: 'Alteração de Feedback'
    },
    DELETE: {
      ID: 11,
      DESCRIPTION: 'Exclusão de Feedback'
    }
  },
  COMMENTS: {
    ADD: {
      ID: 12,
      DESCRIPTION: 'Inclusão de Comentário'
    },
    EDIT: {
      ID: 13,
      DESCRIPTION: 'Alteração de Comentário'
    },
    DELETE: {
      ID: 14,
      DESCRIPTION: 'Exclusão de Comentário'
    }
  },
  PALMS: {
    ADD: {
      ID: 15,
      DESCRIPTION: 'Inclusão de Palmas'
    },
    DELETE: {
      ID: 16,
      DESCRIPTION: 'Exclusão de Palmas'
    }
  }
}
