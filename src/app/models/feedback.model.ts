export interface FeedbackModel {
    idPublicacaoFeedback: number
    idPublicacao: number
    idUsuario: number
    texto: string
    dtCadastro: Date
    dtModificado?: Date
    flagProfessor: boolean
    isDashed: boolean


}
