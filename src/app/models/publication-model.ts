export interface PublicationModel {
    idPublicacao?: number
    idUsuario?: number
    urlPublicacao?: string
    idCurso?: number
    idTurma?: number
    cdStatus?: number
    cdPermissao?: number
    titulo?: string
    reflexao?: string
    dtCadastro?: Date
    dtPublicacao?: Date
    flagFeedback?: number
    flagDesativado: number
    dtModificacao?: Date
    imgPath?: any
    conteudo?: string

}
