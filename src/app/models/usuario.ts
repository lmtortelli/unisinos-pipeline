export class UsuarioModel {
  RA: string
  cdUsuario: string
  email: string
  flagAluno: number
  flagProfessor: number
  idUsuario: number
  nome: string
  privacidade: number
  url: string
  img: any
  cidade: string
  UF: string
  token?: string
  biografia?: string
  lklinkedin?: string
  lktwitter?: string
  lkfacebook?: string
  lklattes?: string
}
export class Usuario {
  usuario: UsuarioModel[]

  constructor(obj: any) {
    this.usuario = obj as UsuarioModel[]
  }
}
