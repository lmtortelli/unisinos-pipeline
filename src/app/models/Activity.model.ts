export class ActivityModel {
    idLogEvento: number
    idUsuario: number
    nomeUsuarioLog: string
    idTipoLogEvento: number
    dtLogEvento: string
    publicacaoTitulo: string
    publicacaoUrl: string
    publicacaoDesativado: number
    publicacaoNomeAutor: string
    publicacaoImgAutor: string
    publicacaoDisciplina: string
    autorUrl: string
    ownActivity: boolean
}
