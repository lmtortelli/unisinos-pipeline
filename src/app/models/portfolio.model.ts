export interface PortfolioModel {
    idUsuario?: number
    cdUsuario?: number
    nome?: string
    url?: string
    img?: string
}
