import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { PublicationModel } from 'src/app/models/publication-model'
import { UsuarioModel } from 'src/app/models/usuario'
import { RestapiService } from 'src/app/services/restapi.service'
import { DomSanitizer, SafeHtml } from '@angular/platform-browser'
import { AppState } from 'src/app/redux/app.state'
import { State } from '@ngrx/store'
import { DefaultService } from 'src/app/services/global/default.service'
import { ModalShareComponent } from 'src/app/component/modal-share/modal-share.component'
import { MatDialogConfig, MatDialog } from '@angular/material'
import { ReplaySubject } from 'rxjs'
import { takeUntil } from 'rxjs/operators'
import { Meta } from '@angular/platform-browser'

@Component({
  selector: 'app-publication-detail',
  templateUrl: './publication-detail.component.html',
  styleUrls: ['./publication-detail.component.scss']
})

export class PublicationDetailComponent implements OnInit {
  @ViewChild('headerPage') public headerPage: ElementRef
  @Input()
  publication: any
  private user: UsuarioModel
  private autor: UsuarioModel
  private hasDraft = false
  private courseName: string
  private className: string
  safeHtml: SafeHtml
  private isLogged = false
  private infoPublication: any = {}
  private feedbackVisible: boolean
  isAutor = false
  show = true
  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1)

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private state: State<AppState>,
    private rest: RestapiService,
    private dialog: MatDialog,
    private dService: DefaultService,
    private meta: Meta,
    private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.show = true
    let cdUsuario
    let titlePost
    localStorage.setItem('activeHeader', '3')

    if (!this.publication) {
      this.route.params.subscribe(params => {
        cdUsuario = params['cdUsuario']
        titlePost = params['titlePost']
        this.getPublicacao(cdUsuario, titlePost)
        this.getUser(cdUsuario)

      })

      this.state.pipe(takeUntil(this.destroyed$)).subscribe(async appState => {
        if (appState.auth.token !== null) {
          this.user = await appState.auth.user.usuario
          this.isLogged = true
        } else {
          this.user = null
          this.isLogged = false
        }
      })
      // FIXME: This value should be TRUE
      this.hasDraft = false
    }
  }

  ngAfterViewChecked(): void {
    try {this.moveHeader()
    } catch (e) {}
  }

  private getPublicacao(cdUsuario: any, titlePost: any) {
    this.rest.getByIdApi('publicacao/urlPost', cdUsuario + '/' + titlePost)
      .pipe(takeUntil(this.destroyed$)).subscribe(res => {
        if (res !== null) {
          this.publication = res as PublicationModel
          // Permissao de visualização
          this.generateTags()
          if (!this.checkPermission(this.publication.cdPermissao, this.publication.cdStatus)) {
            if (localStorage.getItem('professor_flag') != 'true') {
              this.router.navigateByUrl('/meu-portfolio')
            } else {
              this.router.navigateByUrl('/meus-alunos')
            }
          }

          if (this.publication.idCurso != null) {
            this.getCourse(this.publication.idCurso)
          }
          if (this.publication.idTurma != null) {
            this.getDisciplines(this.publication.idTurma)
            this.visibleFeedback(this.publication.idTurma)
          }
          const post = JSON.parse(this.publication.conteudo)
          let postContent = ''
          if (post.length > 0) {
            post.forEach(element => {
              postContent = postContent + '<div id=' + element.id + '>'

              if (element.type === 'text') {
                postContent = postContent + element.value
                postContent = postContent.replace(/<p/g, '<p style="margin-bottom:40px; font-family: \'PT Serif\', serif !important;"')
              } else if (element.type === 'image') {

                postContent = postContent + element.value.replace(/<img style="/g, '<img style="margin-bottom:40px;')
              } else {
                element.value = element.value.replace('SafeValue must use [property]=binding: ', '')
                element.value = element.value.replace('(see http://g.co/ng/security#xss)', '')
                element.value = element.value.replace('iframe', 'iframe style="margin-bottom:40px; margin:auto; display:block; width: 70%; height: 300px;max-width:500px;max-height:300px;margin-bottom:30px;"')
                element.value = element.value.replace('class="mat-card', 'style="margin-bottom: 40px" class="mat-card')
                postContent = postContent + element.value
              }
              postContent = postContent + '</div>'

            })
          }
          this.safeHtml = this.sanitizer.bypassSecurityTrustHtml(postContent)
        } else {
          if (this.user == null) {
            this.router.navigateByUrl('/home')
          } else if (localStorage.getItem('professor_flag') !== 'true') {
            this.router.navigateByUrl('/meu-portfolio')
          } else {
            this.router.navigateByUrl('/meus-alunos')
          }
        }

        this.show = false
      }, error => {
        console.error('Erro ao buscar publicações: ' + JSON.stringify(error))
      })
  }

  moveHeader(): void {
    this.headerPage.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start' })
  }

  private checkPermission(cdPermissao: number, cdStatus: number): boolean {
    if (cdStatus === 0) {
      if (this.publication.idUsuario === (this.user.idUsuario)) {
        return true
      }
    } else {
      switch (cdPermissao) {
        case 0: { // Somente professores
          if (this.user === null) {
            return false
          } else if (this.publication.idUsuario === (this.user.idUsuario)) {
            return true
          } else if (this.user.flagProfessor === 1) {
            return true
          } else {
            return false
          }
          break
        }
        case 1: { // Comunidade Academia, testar login
          if (this.user === null) {
            return false
          } else {
            return true
          }
          break
        }
        case 2:
          return true
      }
    }
  }

  private getUser(cdUsuario: any) {
    this.rest.getByIdApi('usuario/publication-detail', cdUsuario)
      .pipe(takeUntil(this.destroyed$)).subscribe(res => {
        if (!res.user) {
          this.autor = res[0] as UsuarioModel
          if (this.user !== null) {
            if (this.autor.idUsuario === this.user.idUsuario) {
              this.isAutor = true
            }
          }
        }
      }, error => {
        console.error('Erro ao buscar publicações: ' + JSON.stringify(error))
      })
  }

  private getCourse(idCurso: any) {
    this.rest.getByIdApi('curso/get', idCurso)
      .pipe(takeUntil(this.destroyed$)).subscribe(res => {
        if (null != res) {
          this.courseName = res.nome
        }
      }, error => {
        console.error('Erro ao buscar publicações: ' + JSON.stringify(error))
      })
  }

  shareClick() {
    const dialogConfig = new MatDialogConfig()
    dialogConfig.data = this.user
    dialogConfig.disableClose = true
    dialogConfig.autoFocus = false
    const dialogRef = this.dialog.open(ModalShareComponent, dialogConfig)
    dialogRef.componentInstance.title = this.publication.titulo
    dialogRef.componentInstance.desc = this.autor.nome
  }

  private getDisciplines(idTurma: any) {
    this.rest.getByIdApi('disciplina/turma', idTurma)
      .pipe(takeUntil(this.destroyed$)).subscribe(res => {
        if (null != res) {
          this.className = res[0].nome
        }
      }, error => {
        console.error('Erro ao buscar publicações: ' + JSON.stringify(error))
      })
  }

  private visibleFeedback(idTurma: any) {
    const professors = new Set()
    this.rest.getByIdApi('turma/professor', idTurma)
      .pipe(takeUntil(this.destroyed$)).subscribe(res => {
        if (null != res) {
          res.forEach(element => {
            professors.add(element.idUsuarioProfessor)
          })
          if (this.user == null) {
            this.feedbackVisible = false
          } else if ((this.user.idUsuario === this.publication.idUsuario) ||
            (professors.has(this.user.idUsuario))) {
            this.feedbackVisible = true
          } else {
            this.feedbackVisible = false
          }
        }
      }, error => {
        console.error('Erro ao buscar publicações: ' + JSON.stringify(error))
      })
  }

  private editPublication() {
    this.router.navigate(['/nova-publicacao'], { queryParams: { idPublicacao: this.publication.idPublicacao } })
  }

  private remove() {
    this.dService.confimationModal()
      .pipe(takeUntil(this.destroyed$)).subscribe(success => {
        if (success) {
          this.rest.deleteApi('publicacao', this.publication.idPublicacao)
            .pipe(takeUntil(this.destroyed$)).subscribe((data) => {
              this.router.navigateByUrl('/meu-portfolio')
            }, error => {
              console.error('Erro ao deletar feedback')
            })
        }
      })
  }

  generateTags() {
    // default values
    const config = {
      description: 'Leia a publicação ' + this.publication.titulo + ' no Portfólio Unisinos.'
    }

    this.meta.updateTag({ name: 'twitter:card', content: 'summary' })
    this.meta.updateTag({ name: 'twitter:site', content: '@UnisinosPortfolio' })
    this.meta.updateTag({ name: 'twitter:title', content: this.publication.titulo })
    this.meta.updateTag({ name: 'twitter:description', content: config.description })
    this.meta.updateTag({ name: 'twitter:image', content: this.dService.logoUnisinos() })

    this.meta.updateTag({ property: 'og:type', content: 'article' })
    this.meta.updateTag({ property: 'og:site_name', content: 'Unisinos Portfolio' })
    this.meta.updateTag({ property: 'og:title', content: this.publication.titulo })
    this.meta.updateTag({ property: 'og:description', content: config.description })
    this.meta.updateTag({ property: 'og:image', content: this.dService.logoUnisinos() })
    this.meta.updateTag({ property: 'og:url', content: window.location.href})
  }

  ngOnDestroy() {
    this.destroyed$.next(true)
    this.destroyed$.complete()
  }
}
