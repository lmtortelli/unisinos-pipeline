import { Component, OnInit, Input, ViewEncapsulation, ViewChild } from '@angular/core'
import { MatDialogConfig, MatDialog } from '@angular/material'
import { Router, ActivatedRoute } from '@angular/router'
import { RestapiService } from 'src/app/services/restapi.service'
import { DefaultService } from 'src/app/services/global/default.service'
import { ModalInitialPrivacyComponent } from 'src/app/component/modal-initial-privacy/modal-initial-privacy.component'
import { UsuarioModel } from 'src/app/models/usuario'
import { statusPublication, OrderSelect } from 'src/app/app.component'
import { PublicationModel } from 'src/app/models/publication-model'
import { AppState } from 'src/app/redux/app.state'
import { State } from '@ngrx/store'
import { Store } from '@ngrx/store'
import { ACTIVE_MENU } from 'src/app/redux/constants'
import { ModalAtividadeAcademicaComponent } from 'src/app/component/modal-atividade-academica/modal-atividade-academica.component'
import { ScrollStrategyOptions } from '@angular/cdk/overlay'
import { Observable, ReplaySubject } from 'rxjs'
import { ModalShareComponent } from 'src/app/component/modal-share/modal-share.component'
import { takeUntil } from 'rxjs/operators'
import { Meta } from '@angular/platform-browser'

@Component({
  selector: 'app-external-access',
  templateUrl: './external-access.component.html',
  styleUrls: ['./external-access.component.scss']
})
export class ExternalAccessComponent implements OnInit {
  publications: any = []
  user: UsuarioModel = <any>{}
  currentUser: UsuarioModel = <any>{}
  imageUser: string | ArrayBuffer
  courses: string
  hasPublications = false
  hasFilters = false
  statusPublication = `${statusPublication}`
  selects: any = {
    period: {},
    disciplines: {},
    tests: {},
    status: {},
    course: {}
  }
  logMap = new Map()
  professorFlagExternal = false
  offset = 0
  filters: any = {
    periodSelected: 'Todos',
    disciplineSelected: 'Todas',
    testSelected: 'Todas',
    statusSelected: 'Todos',
    courseSelect: 'Todos',
    nameUser: 'Digite o nome ...'
  }


  template: any = {
    linkedin: 'https://www.linkedin.com/in/',
    twitter: 'https://twitter.com/',
    facebook: 'https://www.facebook.com/',
    lattes: 'http://lattes.cnpq.br/'
  }

  visibleMorePublication: boolean
  orderSelect = `${OrderSelect}`
  optionSelected = OrderSelect.Recent
  mensageFilterEmpty = false
  show: boolean

  @Input() urlParcial: string
  users: any[]
  routerInitiate = false
  coursesSet: any = new Set()
  previousLength = 0
  // Professor
  professorUser = false
  idCoursesPublications: any = new Set()

  userProfileProfessor = false

  @ViewChild('timeline')
  private timeline: any
  btnTimeline = true
  professorDisciplines: any = []
  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1)

  constructor(private rest: RestapiService,
    public dService: DefaultService,
    public dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute,
    private state: State<AppState>,
    private meta: Meta,
    private store: Store<AppState>) {
    this.courses = ''
    this.visibleMorePublication = false
  }

  loader() {
    this.show = !this.show
  }

  async ngOnInit() {
    this.show = true
    localStorage.setItem('activeHeader', '3')
    await this.state.pipe(takeUntil(this.destroyed$))
      .pipe(takeUntil(this.destroyed$)).subscribe(async appState => {
        if (appState.auth.token !== null) {
          await this.loadPageWhenLogged(appState)
          if (this.user.img == null) {
            this.imageUser = this.dService.standardUserImage()
          } else {

            this.imageUser = this.user.img
          }
        } else {
          this.loadPageWhenNoLogged(appState)
        }
      })
  }

  shareClick() {
    const dialogConfig = new MatDialogConfig()
    dialogConfig.data = this.user
    dialogConfig.disableClose = true
    dialogConfig.autoFocus = false
    const dialogRef = this.dialog.open(ModalShareComponent, dialogConfig)
    dialogRef.componentInstance.title = this.user.nome
  }

  async loadPageWhenLogged(appState: any) {

    this.currentUser = await appState.auth.user.usuario
    this.professorUser = (localStorage.getItem('professor_flag') === 'true' ? true : false)
    this.route.params.subscribe(async params => {
      if (params['url'] != null) {
        this.getUserParams(params['url']).subscribe(
          success => {
            if (this.user.idUsuario === this.currentUser.idUsuario) {
              if (this.professorUser) {
                this.routerInitiate = true
                this.router.navigate(['/meus-alunos'])
              } else {
                this.router.navigate(['/meu-portfolio'])
                this.routerInitiate = true
              }
            } else {
              this.userProfileProfessor = (this.user.flagProfessor === 1) ? true : false
              if (this.user.img == null) {
                this.imageUser = this.dService.standardUserImage()
              } else {
                this.imageUser = this.user.img
              }
            }

            if (!this.userProfileProfessor && !this.routerInitiate) {
              switch (this.user.privacidade) {
                case 0: {
                  if (this.professorUser) {
                    this.loadPage()
                  } else {
                    this.router.navigate(['/meu-portfolio'])
                  }
                  break
                }

                case 1: {
                  if (this.currentUser != null) {
                    this.loadPage()
                  } else if (this.professorUser) {
                    this.router.navigate(['/meus-alunos'])
                  } else {
                    this.router.navigate(['/meu-portfolio'])
                  }
                  break
                }

                case 2: {
                  this.loadPage()
                }
              }
            } else { // Finish if of student
              this.getDisciplinesForProfessor()
            }
          })
      }
    })
  }

  async loadPageWhenNoLogged(appState: any) {
    this.currentUser = null
    this.professorUser = false
    this.route.params.subscribe(async params => {
      if (params['url'] != null) {
        this.getUserParams(params['url'], false).subscribe(
          success => {
            if (this.user.img == null) {
              this.imageUser = this.dService.standardUserImage()
            } else {
              this.imageUser = this.user.img
            }
            this.loadPage()
          }, error => {
            this.router.navigateByUrl('/home')
          })
      }
    })
  }

  loadPage() {
    this.loader()
    this.getPeriods()
    this.getDisciplines()
    this.getCourse()
    this.getPublicationsByFilter({}, false)
    // this.getPublicationsHasDraft()
    this.getTests()
  }


  getDisciplinesForProfessor() {
    this.rest.getByIdApi('disciplina/usuario', this.user.idUsuario)
      .pipe(takeUntil(this.destroyed$)).subscribe(res => {
        this.professorDisciplines = res
        this.show = false
      }, error => {
        console.error('Erro ao buscar disciplinas: ' + JSON.stringify(error))
      })
  }

  getUserParams(url, logged = true): Observable<boolean> {
    if (logged) {
      return new Observable(observer => {
        this.rest.getByIdApi('usuario/url', url)
          .pipe(takeUntil(this.destroyed$)).subscribe(res => {
            if (res[0] !== undefined) {
              this.user = res[0] as UsuarioModel
              if (this.user.flagProfessor === 1) {
                this.professorFlagExternal = true
              }
              observer.next(true)
              observer.complete()
            } else {
              if (this.professorUser) {
                this.router.navigateByUrl('/meus-alunos')
              } else {
                this.router.navigateByUrl('/meu-portfolio')
              }
            }
          }, error => {
            observer.error(error)
            observer.next(true)
            observer.complete()
            console.error('Erro ao buscar publicações: ' + JSON.stringify(error))
          })
      })
    } else { // Usuario nao logado
      return new Observable(observer => {
        this.rest.getByIdApi('usuario/external-access', url)
          .pipe(takeUntil(this.destroyed$)).subscribe(res => {
            if (res.cdUsuario !== undefined) {
              this.user = res as UsuarioModel
              if (this.user.flagProfessor == 1) {
                this.professorFlagExternal = true
              }
              observer.next(true)
              observer.complete()
            } else {
              this.router.navigate(['/home'])
            }
          }, error => {
            observer.error(error)
            observer.next(true)
            observer.complete()
            this.router.navigate(['/home'])
            console.error('Erro ao buscar publicações: ' + JSON.stringify(error))
          })
      })
    }
  }

  //#region "Student"
  getCourse() {
    this.rest.getByIdApi('usuario/curso', this.user.cdUsuario)
      .pipe(takeUntil(this.destroyed$)).subscribe(res => {
        if (!res.cursos) {
          this.loader()
          return
        }
        res.cursos.forEach(element => {
          if (this.courses === '') {
            this.coursesSet.add(element.nome)
          } else {
            this.coursesSet.add(element.nome)
          }
          this.courses = ''
          let space = ''
          if (this.coursesSet.size > 1) {
            space = ' - '
          }

          this.coursesSet.forEach(el => {
            this.courses = this.courses + el + space
          })
        })
      }, error => {
        this.loader()
        console.error('Erro ao buscar o curso: ' + error)
      })
  }

  getPeriods() {
    this.rest.getByIdApi('periodo/usuario', this.user.cdUsuario)
      .pipe(takeUntil(this.destroyed$)).subscribe(res => {
        this.selects.periods = res
      }, error => {
        this.loader()
        console.error('Erro ao buscar periodos: ' + JSON.stringify(error))
      })
  }

  getDisciplines(payload = {}) {
    this.rest.postApi('disciplina/' + this.user.cdUsuario, payload)
      .pipe(takeUntil(this.destroyed$)).subscribe(res => {
        this.selects.disciplines = res
      }, error => {
        this.loader()
        console.error('Erro ao buscar disciplinas: ' + JSON.stringify(error))
      })
  }

  buildPayload(): any {
    const payload = {
      idPeriodoLetivo: this.filters.periodSelected.idPeriodoLetivo,
      idDisciplina: this.filters.disciplineSelected.idDisciplina,
      idAvaliacao: this.filters.testSelected.idAvaliacao,
      cdStatus: this.filters.statusSelected
    }


    if (this.filters.periodSelected == 'Todos') {
      delete payload.idPeriodoLetivo
    }
    if (this.filters.disciplineSelected == 'Todas') {
      delete payload.idDisciplina
    }
    if (this.filters.testSelected == 'Todas') {
      delete payload.idAvaliacao
    }
    if (this.filters.statusSelected == 'Todos') {
      delete payload.cdStatus
    }

    return payload
  }


  validateCdPermissao(payload = {}) {
    if (this.professorUser) {
      payload['cdPermissao'] = 0
    } else if (this.currentUser != null) {
      payload['cdPermissao'] = 1
    } else {
      payload['cdPermissao'] = 2
    }

    return payload
  }

  showSpinner(act: boolean) {
    this.show = act
  }

  getTests(payload = {}) {
    this.rest.postApi('avaliacao/' + this.user.cdUsuario, payload)
      .pipe(takeUntil(this.destroyed$)).subscribe(res => {
        this.filters.testSelected = 'Todas'
        this.selects.tests = res
      }, error => {
        this.loader()
        console.error('Erro ao buscar avaliações: ' + JSON.stringify(error))
      })
  }


  submitFilters() {
    this.offset = 0
    this.show = true
    this.logMap.clear()
    const payload = this.buildPayload()
    this.getPublicationsByFilter(payload, true)
  }


  getPublicationsByFilter(payload: any, update = false) {
    this.validateCdPermissao(payload)
    this.rest.postApi('publicacao/aluno/filtro/' + this.user.idUsuario + '/' + this.offset, payload)
    .pipe(takeUntil(this.destroyed$)).subscribe(res => {
      this.UpdatePublicationsByFilter(res, update)
    }, error => {
      this.loader()
      console.error('Erro ao buscar publicações: ' + JSON.stringify(error))
    })
  }

  addingPublication(publication: any) {
    if (!this.logMap.has(publication.idPublicacao)) {
      this.hasFilters = true
      this.mensageFilterEmpty = false
      this.hasPublications = true
      this.logMap.set(publication.idPublicacao, publication)
    }
  }

  UpdatePublicationsByFilter(res: any, update= false) {
    if (res !== null && res.length > 0) {
      this.visibleMorePublication = true
      res.forEach(element => {
        if (element.cdStatus != statusPublication.Draft) {
          this.mensageFilterEmpty = false
          this.hasPublications = true
          const publ = <PublicationModel>element
          if (!this.logMap.has(publ.idPublicacao)) {
            this.hasFilters = true
            this.logMap.set(publ.idPublicacao, publ)
          }
        } else {
          this.visibleMorePublication = false
          if (this.logMap.size == 0) {
            this.mensageFilterEmpty = update
            this.hasPublications = false
          }
        }
      })
      this.onChangeOrder(this.optionSelected)
    } else {
      this.visibleMorePublication = false
      if (this.logMap.size == 0) {
        this.mensageFilterEmpty = update
        this.hasPublications = false
      }
    }
    this.show = false
  }

  onChangePeriods(event) {
    const payload = { idPeriodoLetivo: event.idPeriodoLetivo }
    this.getDisciplines(payload)
  }

  onChangeCurso() {
    const payload = {
      idCurso: this.filters.courseSelect.idCurso,
      idPeriodoLetivo: this.filters.periodSelected.idPeriodoLetivo
    }
    this.getDisciplines(payload)
  }

  onChangeDisciplines(event) {
    const payload = { idDisciplina: event.idDisciplina }
    this.getTests(payload)
  }


  openDialog(): void {
    const dialogConfig = new MatDialogConfig()
    dialogConfig.disableClose = true
    dialogConfig.autoFocus = true
    const dialogRef = this.dialog.open(ModalAtividadeAcademicaComponent, dialogConfig)
    dialogRef.componentInstance.user = this.user
  }

  onChangeOrder(event) {
    if (this.publications.length !== this.logMap.size) {
      this.publications = []
      this.logMap.forEach(val => {
        this.publications.push(val)
      })
    }
    switch (event) {
      case OrderSelect.Recent: {
        this.publications.sort((a, b) => (a.dtCadastro < b.dtCadastro) ? 1 : -1)
        break
      }
      case OrderSelect.Old: {
        this.publications.sort((a, b) => (a.dtCadastro > b.dtCadastro) ? 1 : -1)
        break
      }
      case OrderSelect.AlfaOrder: {
        this.publications.sort((a, b) => a.titulo.localeCompare(b.titulo))
        break
      }
      case OrderSelect.AlfaInverseOorder: {
        this.publications.sort((a, b) => b.titulo.localeCompare(a.titulo))
        break
      }
    }
  }

  UpdateMorePublication() {
    this.show = true
    this.offset += 8
    const payload = this.buildPayload()
    this.getPublicationsByFilter(payload, false)

  }

  visibleButtonMorePublication() {
    if (this.previousLength === this.publications.length) {
      this.visibleMorePublication = false
    } else {
      if ((this.publications.length - this.previousLength) !== 8) {
        this.visibleMorePublication = false
      } else {
        this.visibleMorePublication = true
      }
      this.previousLength = this.publications.length
    }
  }

  generateTags() {
    // default values
    const config = {
      title : this.user.nome,
      description: 'Veja meu perfil e publicações no Portfólio Unisinos.',
      image : this.dService.logoUnisinos()
    }

    this.meta.updateTag({ name: 'twitter:card', content: 'summary' })
    this.meta.updateTag({ name: 'twitter:site', content: '@UnisinosPortfolio' })
    this.meta.updateTag({ name: 'twitter:title', content: config.title })
    this.meta.updateTag({ name: 'twitter:description', content: config.description })
    this.meta.updateTag({ name: 'twitter:image', content: config.image })

    this.meta.updateTag({ property: 'og:type', content: 'article' })
    this.meta.updateTag({ property: 'og:site_name', content: 'Unisinos Portfolio' })
    this.meta.updateTag({ property: 'og:title', content: config.title })
    this.meta.updateTag({ property: 'og:description', content: config.description })
    this.meta.updateTag({ property: 'og:image', content: config.image })
    this.meta.updateTag({ property: 'og:url', content: window.location.href})
  }



  ngOnDestroy() {
    this.destroyed$.next(true)
    this.destroyed$.complete()
  }
}
