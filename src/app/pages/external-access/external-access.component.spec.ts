import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExternalAccessComponent } from './external-access.component';

describe('ExternalAccessComponent', () => {
  let component: ExternalAccessComponent;
  let fixture: ComponentFixture<ExternalAccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExternalAccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExternalAccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
