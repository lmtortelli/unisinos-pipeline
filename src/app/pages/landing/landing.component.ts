import { Component, OnInit, ElementRef, ViewChild } from '@angular/core'
import { RestapiService } from 'src/app/services/restapi.service'
import { Store } from '@ngrx/store'
import { AppState } from 'src/app/redux/app.state'
import { LOGOUT } from 'src/app/redux/constants'

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {
  @ViewChild('about') public about: ElementRef
  @ViewChild('mySidenav') mySidenav: ElementRef
  @ViewChild('howTo') public how: ElementRef
  @ViewChild('faq') public faq: ElementRef
  @ViewChild('destaques') public destaques: ElementRef
  @ViewChild('header') public header: ElementRef
  constructor(
    private rest: RestapiService,
    private store: Store<AppState>) { }

  menuButtonClick = false
  openLoginMenu = false

  ngOnInit() {
    localStorage.clear()
    this.store.dispatch({ type: LOGOUT, payload: 0})
  }

  public openNav() {
    this.menuButtonClick = true
  }

  public closeNav() {
    this.menuButtonClick = false
  }

  public moveAbout(): void {
    this.about.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'end', inline: 'start' })
    this.menuButtonClick = false

  }
  public moveHow(): void {
    this.how.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start' })
    this.menuButtonClick = false
  }
  public moveDestaque(): void {
    this.destaques.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start' })
    this.menuButtonClick = false
  }

  public moveFaq(): void {
    this.faq.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start' })
    this.menuButtonClick = false
  }

  public moveHeader() {
    this.header.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start' })
    this.openLoginMenu = !this.openLoginMenu
  }
}
