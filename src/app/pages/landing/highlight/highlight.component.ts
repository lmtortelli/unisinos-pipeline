import { Component, OnInit } from '@angular/core'
import { statusPublication } from 'src/app/app.component'
import { PublicationModel } from 'src/app/models/publication-model'
import { RestapiService } from 'src/app/services/restapi.service'
import { PortfolioModel } from 'src/app/models/portfolio.model'
import { ReplaySubject } from 'rxjs'
import { takeUntil } from 'rxjs/operators'

@Component({
  selector: 'app-highlight',
  templateUrl: './highlight.component.html',
  styleUrls: ['./highlight.component.scss']
})
export class HighlightComponent implements OnInit {
  publications: any = []
  portfolios: any = []
  private show = true
  private statusPublication = `${statusPublication}`
  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1)

  constructor(
    private rest: RestapiService
  ) { }

  ngOnInit() {
    this.show = true
    this.getPublicacoes()
    this.getPortfolios()

  }


  getPublicacoes() {
    this.rest.getApi('publicacao/get/landing').pipe(takeUntil(this.destroyed$)).subscribe(res => {
      if (res.length > 0) {
        res.forEach(element => {
          if (element.cdStatus !== statusPublication.Draft) {
            this.publications.push(<PublicationModel>element)
          }
        })
      }
      this.show = false
    }, error => {
      this.show = false
      console.error('Erro ao buscar publicações: ' + JSON.stringify(error))
    })
  }

  getPortfolios() {
    this.rest.getApi('usuario/portfolio/landing').pipe(takeUntil(this.destroyed$)).subscribe(res => {
      if (res != null && res.length >= 1) {
        res.forEach(element => {
          this.portfolios.push(<PortfolioModel>element)
        })
      }
      this.show = false
    }, error => {
      this.show = false
      console.error('Erro ao buscar publicações: ' + JSON.stringify(error))
    })
  }

  ngOnDestroy() {
    this.destroyed$.next(true)
    this.destroyed$.complete()
  }
}
