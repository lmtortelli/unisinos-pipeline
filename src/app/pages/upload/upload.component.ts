import { Component, OnInit } from '@angular/core'
import { RestapiService } from 'src/app/services/restapi.service'
import { UploadServiceService } from 'src/app/services/upload/upload-service.service'

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {
  displayedColumns: string[] = ['cdpais', 'ddd', 'fone']
  showTable = false
  dataTable: any = []
  model: any = {}
  errorBool = false
  errorMessage: any
  file: any
  fileContent: any[] = []
  idUpdateCampanha: any
  constructor(private upload: UploadServiceService) { }

  ngOnInit() {
  }
  /*- checks if word exists in array -*/
  isInArray(array, word) {
    return array.indexOf(word.toLowerCase()) > -1
  }
  uploadFile(e) {
    this.file = e.target.files[0]
    if (this.file) {
      let allowedExtensions =
        ['xlsx', 'xlsm', 'xls', 'xla', 'xlt', 'xlm', 'csv', 'txt']
      const fileExtension = this.file.name.split('.').pop()

      if (this.isInArray(allowedExtensions, fileExtension)) {
        this.model.fileName = this.file.name
        this.uploadDocument(this.file)
      } else {
        this.dataTable = []
        this.showTable = false
      }
    }
  }

  uploadDocument(file) {
    const reader = new FileReader()
    reader.readAsDataURL(file)
    const self = this
    reader.onload = function (e) {
      self.model.file = reader.result
    }

    const fileReader = new FileReader()
    fileReader.onload = (e) => {
      this.dataTable = this.csvJSON(fileReader.result)
      if (this.dataTable != undefined) {
        this.showTable = true
      } else {
        this.dataTable = []
        this.showTable = false
      }
      this.upload.uploadFile(this.dataTable).subscribe(data => {
        if (data.error == true) {
          this.errorBool = true
          this.errorMessage = data.msg
        }
      }, err => {
      })
      this.getAlunos(this.dataTable)
    }
    fileReader.readAsText(this.file)

  }

  csvJSON(csv) {
    const lines = csv.toString().split('\r\n')
    const result = []
    let headers

    for (let i = 0; i < lines.length; i++) {
      if (i == 0) {
        headers = lines[0].split(';')
      } else {
        const obj = {}
        const currentline = lines[i].split(';')
        if (currentline) {
          for (let j = 0; j < headers.length; j++) {
            obj[headers[j]] = currentline[j]
          }
        }
        result.push(obj)
      }
    }

    return result
  }

  getAlunos(dados) {
    const alunos = []
    dados.forEach(aluno => {
      const result = alunos.some((user, index, array) => user.CODUSUARIO_ALUNO === aluno.CODUSUARIO_ALUNO)
      if (!alunos.some((user, index, array) => user.CODUSUARIO_ALUNO === aluno.CODUSUARIO_ALUNO)) {
        alunos.push(aluno)
      } else {
        return
      }
    })
    this.dataTable = alunos
  }
}
