import { Component, OnInit, Input, ViewChild } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { RestapiService } from 'src/app/services/restapi.service'
import { UsuarioModel } from 'src/app/models/usuario'
import { MatTabChangeEvent, MatTabGroup, MatSnackBar } from '@angular/material'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { MatStepper, MatVerticalStepper } from '@angular/material/stepper'
import { StepperSelectionEvent } from '@angular/cdk/stepper'
import { PROFESSOR_USER, ACTIVE_MENU } from 'src/app/redux/constants'
import { Store } from '@ngrx/store'
import { AppState } from 'src/app/redux/app.state'
import { takeUntil } from 'rxjs/operators'
import { ReplaySubject } from 'rxjs'

@Component({
  selector: 'app-register-profile',
  templateUrl: './register-profile.component.html',
  styleUrls: ['./register-profile.component.scss']
})
export class RegisterProfileComponent implements OnInit {
  user: UsuarioModel = <any>{}
  imagePath: string
  message: string
  disable = false
  regexUrl = '^(http[s]?:\\/\\/){0,1}(www\\.){0,1}[a-zA-Z0-9\\.\\-]+\\.[a-zA-Z]{2,5}[\\.]{0,1}$'
  @Input() urlParcial: string

  indexTab = 0

  biography: string
  template: any = {
    linkedin: 'https://www.linkedin.com/in/',
    twitter: 'https://twitter.com/',
    facebook: 'https://www.facebook.com/',
    lattes: 'http://lattes.cnpq.br/'
  }
  links: any = {
    linkedin: this.template.linkedin,
    twitter: this.template.twitter,
    facebook: this.template.facebook,
    lattes: this.template.lattes
  }
  isLinear = false
  firstFormGroup: FormGroup
  secondFormGroup: FormGroup
  TFormGroup: FormGroup
  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1)

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private rest: RestapiService,
    private _formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private store: Store<AppState>) {

  }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('currentUser'))
    if (this.user.url != null) {
      if (this.user.flagProfessor == 0) {
        this.router.navigateByUrl('/meu-portfolio')
      } else {
        this.router.navigateByUrl('/meus-alunos')
      }
      this.buildCombo()

    }
    this.urlParcial = this.user.cdUsuario.toLowerCase()
    this.onChange(this.urlParcial)

    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    })

    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    })
    this.TFormGroup = this._formBuilder.group({
      T: ['', Validators.required]
    })
  }

  buildCombo() {
    this.links.linkedin += (this.user.lklinkedin) ? this.user.lklinkedin : ''
    this.links.twitter += (this.user.lktwitter) ? this.user.lktwitter : ''
    this.links.facebook += (this.user.lkfacebook) ? this.user.lkfacebook : ''
    this.links.lattes += (this.user.lklattes) ? this.user.lklattes : ''
    this.biography = this.user.biografia
  }

  onChange(value) {
    // tslint:disable-next-line: triple-equals
    if (value == '') {
      this.disable = true
      this.imagePath = 'assets/img/erro.png'
      this.message = 'Informe um endereço.'
    } else {
      this.rest.getByIdApi('usuario/url', value)
        .pipe(takeUntil(this.destroyed$)).subscribe(
          usuario => {
            if (usuario.length > 0) {
              this.disable = true
              this.imagePath = 'assets/img/erro.png'
              this.message = 'Endereço já utilizado. Tente outro.'
            } else {
              this.disable = false
              this.user.url = value
              this.imagePath = 'assets/img/ok.png'
              this.message = 'Endereço Disponível!'
            }
          },
          error => console.error('Erro no retorno do getUsuario: ' + error)
        )
    }

  }

  logout(): void {
    localStorage.clear()
    this.router.navigate(['/home'])
  }

  tabChanged = (tabChangeEvent: MatTabChangeEvent): void => {
    this.indexTab = tabChangeEvent.index
  }

  public selectionChanged($event?: StepperSelectionEvent): void {
    this.indexTab = $event.selectedIndex
  }

  onSubmit() {
    const payload = {
      lklinkedin: this.links.linkedin.replace(this.template.linkedin, ''),
      lktwitter: this.links.twitter.replace(this.template.twitter, ''),
      lkfacebook: this.links.facebook.replace(this.template.facebook, ''),
      lklattes: this.links.lattes.replace(this.template.lattes, ''),
      biografia: this.biography,
      url: this.urlParcial
    }

    if (payload.lklinkedin == null) {
      delete payload.lklinkedin
    }
    if (payload.lktwitter == null) {
      delete payload.lktwitter
    }
    if (payload.lkfacebook == null) {
      delete payload.lkfacebook
    }
    if (payload.lklattes == null) {
      delete payload.lklattes
    }
    if (payload.biografia == null) {
      delete payload.biografia
    }
    if (payload.url == null) {
      delete payload.url
    }

    this.rest.updateApi('usuario', payload, this.user.cdUsuario)
      .pipe(takeUntil(this.destroyed$)).subscribe(
        res => {
          localStorage.setItem('currentUser', JSON.stringify(res))
          localStorage.setItem('firstTime', 'true')
          const prof = localStorage.getItem('professor_flag')
          if (prof === 'false') {
            this.router.navigate(['/meu-portfolio'])
          } else {
            this.router.navigateByUrl('/meus-alunos')
          }
        },
        error => {
          console.error('Erro no retorno do getUsuario: ' + error)
        }
      )
  }

  nextOrOnSubmit(stepper: any) {

    if (this.indexTab !== 2) {
      this.indexTab++
    } else {
      const payload = {
        lklinkedin: this.links.linkedin.replace(this.template.linkedin, ''),
        lktwitter: this.links.twitter.replace(this.template.twitter, ''),
        lkfacebook: this.links.facebook.replace(this.template.facebook, ''),
        lklattes: this.links.lattes.replace(this.template.lattes, ''),
        biografia: this.biography,
        url: this.urlParcial
      }

      if (payload.lklinkedin == null) {
        delete payload.lklinkedin
      }
      if (payload.lktwitter == null) {
        delete payload.lktwitter
      }
      if (payload.lkfacebook == null) {
        delete payload.lkfacebook
      }
      if (payload.lklattes == null) {
        delete payload.lklattes
      }
      if (payload.biografia == null) {
        delete payload.biografia
      }
      if (payload.url == null) {
        delete payload.url
      }

      this.rest.updateApi('usuario', payload, this.user.cdUsuario)
        .pipe(takeUntil(this.destroyed$)).subscribe(
          res => {
            localStorage.setItem('currentUser', JSON.stringify(res))
            localStorage.setItem('firstTime', 'true')
            const prof = localStorage.getItem('professor_flag')
            if (prof === 'false') {
              this.router.navigate(['/meu-portfolio'])
            } else {
              this.router.navigateByUrl('/meus-alunos')
            }
          },
          error => {
            console.error('Erro no retorno do getUsuario: ' + error)
          }
        )
    }

  }
  backTab(stepper: any) {
    this.indexTab--
  }


  errorSnackBar(message) {
    this.snackBar.open(message, null, {
      duration: 3500
    })
  }

  jumpForm() {
    const payload = { url: this.urlParcial }
    this.rest.updateApi('usuario', payload, this.user.cdUsuario)
      .pipe(takeUntil(this.destroyed$)).subscribe(
        res => {
          localStorage.setItem('currentUser', JSON.stringify(res))
          localStorage.setItem('firstTime', 'true')
          const prof = localStorage.getItem('professor_flag')
          if (prof === 'false') {
            this.router.navigate(['/meu-portfolio'])
          } else {
            this.router.navigate(['/meus-alunos'])
          }
        }, error => {
          console.error('Erro no retorno do getUsuario: ' + error)
        })
  }

  ngOnDestroy() {
    this.destroyed$.next(true)
    this.destroyed$.complete()
  }
}
