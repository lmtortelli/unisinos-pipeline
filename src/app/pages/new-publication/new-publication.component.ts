import { Component, OnInit, ViewEncapsulation, Input, ViewChild, ElementRef } from '@angular/core'
import { MatDialogConfig, MatDialog, MatSnackBar } from '@angular/material'
import { UsuarioModel } from 'src/app/models/usuario'
import { ModalConfigPostComponent } from 'src/app/component/modal-config-post/modal-config-post.component'
import { Router, ActivatedRoute } from '@angular/router'
import { DomSanitizer } from '@angular/platform-browser'
import { RestapiService } from 'src/app/services/restapi.service'
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop'
import { EmbedVideoService } from 'ngx-embed-video'
import { ModalEmbedComponent } from 'src/app/component/modal-embed/modal-embed.component'
import { HttpClient, HttpParams } from '@angular/common/http'
import { AppState } from 'src/app/redux/app.state'
import { State } from '@ngrx/store'
import { PublicationModel } from 'src/app/models/publication-model'
import { throwError, ReplaySubject } from 'rxjs'
import { DefaultService } from 'src/app/services/global/default.service'
import { takeUntil } from 'rxjs/operators'

@Component({
  selector: 'app-new-publication',
  templateUrl: './new-publication.component.html',
  styleUrls: ['./new-publication.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class NewPublicationComponent implements OnInit {
  private idPublicacao: number
  @ViewChild('headerPage') public headerPage: ElementRef

  private user: UsuarioModel = <any>{}
  showComponent = false
  post: any = []
  postConfig: any = {}
  title: string = null
  val = 0
  placeholderVar = ' '
  textPost = ''
  idCurso: number = null
  idTurma: number = null
  cdStatus: number
  cdPermissao: number
  reflexao = ''
  isDraft: boolean
  publication: any = null
  payloadPost: any
  urlPublicacao: string
  show = true
  draftSaved = false
  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1)

  @ViewChild('imageInput') inputImages: ElementRef

  constructor(private dialog: MatDialog, private route: ActivatedRoute,
    private sanitizer: DomSanitizer, private rest: RestapiService,
    private embedService: EmbedVideoService, private snackBar: MatSnackBar, private http: HttpClient,
    private state: State<AppState>, private router: Router, private dService: DefaultService) {
    this.cdStatus = 0
    this.cdPermissao = 0
  }

  ngOnInit() {
    this.route.queryParams.pipe(takeUntil(this.destroyed$)).subscribe(params => {
      this.idPublicacao = params['idPublicacao']
    })

    this.state.pipe(takeUntil(this.destroyed$)).subscribe(appState => {
      if (appState.auth.token != null) {
        this.user = appState.auth.user.usuario
      } else {
        this.router.navigateByUrl('/home')
      }
    })

    if (this.idPublicacao != null) {
      this.getPublicacao(this.idPublicacao)
    } else {
      this.postConfig = {
        idTurma: null,
        idAvaliacao: null,
        copyright: true,
        privacy: 0,
        reflection: '',
        isDraft: true
      }
      this.show = false
    }
    this.isDraft = true
    this.getCourse()
  }

  private getPublicacao(idPublicacao: any) {
    this.rest.getByIdApi('publicacao', idPublicacao)
      .pipe(takeUntil(this.destroyed$)).subscribe(res => {
        if (!res.publicacao) {
          this.publication = res as PublicationModel
          this.title = this.publication[0].titulo
          this.post = JSON.parse(this.publication[0].conteudo)
          this.postConfig = {
            idTurma: this.publication[0].idTurma,
            idAvaliacao: this.publication[0].idAvaliacao,
            copyright: true,
            privacy: this.publication[0].cdPermissao,
            reflection: this.publication[0].reflexao,
            isDraft: this.publication[0].cdStatus === 0 ? true : false
          }
          if (this.publication[0].idUsuario !== this.user.idUsuario) {
            this.router.navigateByUrl('/meu-portfolio')
          }
          this.show = false
        }
      }, error => {
        console.error('Erro ao buscar publicações: ' + JSON.stringify(error))
      })
  }

  onSubmitPublication() {
    if (this.verifyInfo()) {
      this.openDialog()
    }
  }

  saveDraft() {
    if (this.verifyInfo()) {
      this.isDraft = true
      this.postConfig.isDraft = true
      const payload = this.buildPayload()
      this.postPublication(payload)
    }
  }

  onPrevia() {
    if (this.verifyInfo()) {
      this.isDraft = true
      this.postConfig.isDraft = true
      const payload = this.buildPayload()
      this.postPublication(payload, true)
    }
  }

  moveHeader(): void {
    this.headerPage.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start' })
  }

  buildPayload() {
    let postContent = ''
    let firstImage = ''
    if (this.post.length > 0) {
      this.post.forEach(element => {
        postContent = postContent + '<div id=' + element.id + '>'
        if (element.type === 'text') {
          postContent = postContent + element.value
        } else if (element.type === 'image') {
          if (firstImage === '') {
            firstImage = element.html
          }
          postContent = postContent + element.value
        } else {
          postContent = postContent + element.value
        }
        postContent = postContent + '</div>'
      })
    }
    if (this.title != null) {
      this.urlPublicacao = this.user.cdUsuario.toLowerCase() + '/' + this.title.replace(/\s/g, '-').toLowerCase()
      this.urlPublicacao = this.urlPublicacao.replace(/\?/g, '').toLowerCase()
    }
    this.payloadPost = [{
      idUsuario: this.user.idUsuario,
      urlPublicacao: this.urlPublicacao,
      idCurso: this.idCurso,
      idTurma: this.postConfig.idTurma,
      cdStatus: this.isDraft ? 0 : 1,
      cdPermissao: this.postConfig.privacy,
      titulo: this.title,
      idAvaliacao: this.postConfig.idAvaliacao,
      conteudo: JSON.stringify(this.post),
      imgPath: firstImage !== '' ? firstImage : null,
      reflexao: this.postConfig.reflection,
      dtCadastro: null,
      dtPublicacao: null,
      dtModificacao: null
    }]

    return this.payloadPost
  }

  async postPublication(payload, previa = false) {
    if (this.publication != null) {
      delete payload[0].dtCadastro
      if (!this.isDraft) {
        payload[0].dtPublicacao = new Date().toJSON().slice(0, 19).replace('T', ' ')
      } else {
        delete payload[0].dtPublicacao
        payload[0].dtModificacao = new Date().toJSON().slice(0, 19).replace('T', ' ')
      }
      this.rest.updateApi('publicacao', payload, this.publication[0].idPublicacao)
        .pipe(takeUntil(this.destroyed$)).subscribe(
          res => {
            this.publication[0] = res
            if (previa) {
              window.open('/' + this.publication[0].urlPublicacao, '_blank')
            } else {
              this.draftSaved = true
            }
          }, error => {
            console.error('Erro no retorno do getUsuario: ' + error)
          })
    } else {
      this.postingPublication(payload, previa)
    }
  }

  postingPublication(payload: any, previa = false) {
    this.rest.getByIdApi('publicacao/url', this.urlPublicacao)
      .pipe(takeUntil(this.destroyed$)).subscribe(res => {
        if (res.count !== 0) {
          const qtdPubl = res.count
          const urlPublicacao = this.urlPublicacao + '-' + (qtdPubl + 1)
          payload['urlPublicacao'] = urlPublicacao
        } else {
          payload['urlPublicacao'] = this.urlPublicacao
        }
        this.rest.postApi('publicacao', payload)
          .pipe(takeUntil(this.destroyed$)).subscribe(res_post => {
            this.publication = res_post
            if (previa) {
              window.open('/' + this.publication[0].urlPublicacao, '_blank')
              // this.router.navigateByUrl('/' + payload['urlPublicacao'])
            } else {
              this.draftSaved = true
            }
          }, error => {
            console.error('Erro no retorno do getUsuario: ' + error)
          })
      },
        error => {
          console.error('Erro na criacao da URL: ' + error)
        }
      )
  }

  addComponents() {
    this.showComponent = true
  }

  otherComponents(type: string) {
    this.draftSaved = false
    switch (type) {
      case 'text':
        this.post.push({
          id: this.val++,
          value: '',
          html: this.val,
          type: 'text'
        })
        break
      case 'image':
        break
      case 'video':
        this.openDialogEmbed('video')
        break
      case 'embed':
        this.openDialogEmbed('embed')
        break
      default:
        break
    }
    this.showComponent = false
  }

  getImageBase64(image) {
    Array.from(image.files).forEach(element => {
      const file: File = element as File
      const reader: FileReader = new FileReader()
      reader.onloadend = (e) => {
        this.post.push({
          id: this.val++,
          value: '<img style="display: block; margin: auto auto 40px auto; max-width: 100%" src="' + reader.result + '">',
          html: reader.result,
          type: 'image'
        })
      }
      reader.readAsDataURL(file)
    })
  }

  dropped(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.post, event.previousIndex, event.currentIndex)
  }

  openDialogEmbed(type): void {
    const dialogConfig = new MatDialogConfig()
    dialogConfig.disableClose = true
    dialogConfig.autoFocus = true
    dialogConfig.data = type
    const dialogRef = this.dialog.open(ModalEmbedComponent, dialogConfig)
    dialogRef.afterClosed().subscribe(result => {
      if (dialogRef.componentInstance.urlEmbed !== '') {
        try {
          if (type === 'video') {
            if (this.embedService.embed(result) != undefined) {
              this.post.push({
                id: this.val++,
                value: '<div style="display: block; margin: auto; max-width: 100%;">' + this.embedService.embed(result) + '</div>',
                html: this.embedService.embed(result).changingThisBreaksApplicationSecurity,
                type: 'video'
              })
            } else {
              this.errorSnackBar('URL inválida, tente novamente')
            }
          } else {
            const params = new HttpParams().set('key', '5c9e14ddb2f93fbfdab7e3983009e7d488d946a8de066').set('q', result)
            let linkpreview: any
            this.http.get('http://api.linkpreview.net/', { params: params })
              .pipe(takeUntil(this.destroyed$)).subscribe(res => {
                linkpreview = res
                this.post.push({
                  id: this.val++,
                  value: '<div class="mat-card ng-star-inserted"> <div class="cut-text mat-card-content"> <div class="row"> <div class="img-container ng-star-inserted col-md-2"> <img class="mat-card-image" src="' + linkpreview.image + '"> </div><a href="' + linkpreview.url + '" target="_blank" class="col-md-9"> <div class=""> <div class="mat-card-title"> ' + linkpreview.title + ' </div> <div class="mat-card-subtitle"> ' + linkpreview.description + ' </div> </div></a> </div></div> </div>',
                  html: linkpreview,
                  type: 'embed'
                })
              })
          }
        } catch (error) {
          console.error(error)
          this.errorSnackBar('URL inválida, tente novamente')
        }
      }
    })
  }

  errorSnackBar(message) {
    this.snackBar.open(message, null, {
      duration: 3500
    })
  }

  verifyInfo() {
    if (this.title === '' || this.title == null) {
      this.errorSnackBar('Por favor, informe o título da publicação!')
      return false
    }
    if (this.title.trim() === '') {
      this.errorSnackBar('Por favor, informe o título da publicação!')
      return false
    }
    if (this.post.length === 0) {
      this.errorSnackBar('Por favor, insira algum conteúdo!')
      return false
    }
    return true
  }
  deleteItem(item) {
    const index: number = this.post.indexOf(item)
    if (index !== -1) {
      this.post.splice(index, 1)
    }
    this.inputImages.nativeElement.value = ''


  }

  openDialog(): void {
    const dialogConfig = new MatDialogConfig()
    dialogConfig.data = {
      payload: this.buildPayload(),
      postConfig: this.postConfig,
      draftId: this.publication != null ? this.publication[0].idPublicacao : null
    }
    dialogConfig.disableClose = true
    dialogConfig.autoFocus = true
    const dialogRef = this.dialog.open(ModalConfigPostComponent, dialogConfig)
    dialogRef.afterClosed().subscribe(result => {
      this.payloadPost = dialogRef.componentInstance.post
      this.postConfig = dialogRef.componentInstance.postConfig
      if (this.publication == null) {
        this.publication = dialogRef.componentInstance.publication
      }
    })
  }

  getCourse() {
    this.rest.getByIdApi('usuario/curso', this.user.cdUsuario)
      .pipe(takeUntil(this.destroyed$)).subscribe(res => {
        if (!res.cursos) {
          return
        }
        this.idCurso = res.cursos[0].idCurso
      }, error => {
        console.error('Erro ao buscar o curso: ' + error)
      })
  }

  remove() {
    if (this.idPublicacao != null) {
      this.dService.confimationModal().subscribe(success => {
        if (success) {
          this.rest.deleteApi('publicacao', this.idPublicacao)
            .pipe(takeUntil(this.destroyed$)).subscribe((data) => {
              if (this.user.flagProfessor == 0) {
                this.router.navigateByUrl('/meu-portfolio')
              } else {
                this.router.navigate(['/meus-alunos'])
              }
            }, error => {
              console.error('Erro ao deletar feedback')
            })
        }
      })
    } else {
      if (this.user.flagProfessor == 0) {
        this.router.navigateByUrl('/meu-portfolio')
      } else {
        this.router.navigate(['/meus-alunos'])
      }
    }
  }
  ngOnDestroy() {
    this.destroyed$.next(true)
    this.destroyed$.complete()
  }
}
