import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { RestapiService } from 'src/app/services/restapi.service'
import { UsuarioModel } from 'src/app/models/usuario'
import { AppState } from 'src/app/redux/app.state'
import { State } from '@ngrx/store'
import { Store } from '@ngrx/store'
import { ACTIVE_MENU } from 'src/app/redux/constants'
import { ModalShareComponent } from 'src/app/component/modal-share/modal-share.component'
import { MatDialog, MatDialogConfig } from '@angular/material'
import { takeUntil } from 'rxjs/operators'
import { ReplaySubject } from 'rxjs'
import { Meta } from '@angular/platform-browser'
import { DefaultService } from 'src/app/services/global/default.service'

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {


  user: UsuarioModel = <any>{}
  imageUser: string | ArrayBuffer
  show = true
  professorUser = false
  professorDisciplines: any = []
  template: any = {
    linkedin: 'https://www.linkedin.com/in/',
    twitter: 'https://twitter.com/',
    facebook: 'https://www.facebook.com/',
    lattes: 'http://lattes.cnpq.br/'
  }
  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1)

  constructor(private rest: RestapiService,
    private router: Router,
    private dialog: MatDialog,
    private meta: Meta,
    private dService: DefaultService,
    private store: Store<AppState>,
    private state: State<AppState>) {
  }

  ngOnInit() {
    this.state.pipe(takeUntil(this.destroyed$)).subscribe(async appState => {
      if (appState.auth.token != null) {
        this.user = await appState.auth.user.usuario
        if (this.user.url == null) {
          this.router.navigateByUrl('/cadastro')
        }
        if (this.user.img == null) {
          this.imageUser = 'assets/img/perfil.png'
        } else {
          this.imageUser = this.user.img
        }
        this.generateTags()
      } else {
        this.router.navigateByUrl('/home')
      }
    })

    this.store.dispatch({ type: ACTIVE_MENU, payload: 2 })
    this.state.pipe(takeUntil(this.destroyed$)).subscribe(async appState => {
      this.professorUser = await appState.view.isProfessor
      this.getDisciplinesForProfessor()
    })

  }

  getDisciplinesForProfessor() {
    this.rest.getByIdApi('disciplina/usuario', this.user.idUsuario)
      .pipe(takeUntil(this.destroyed$)).subscribe(res => {
        this.professorDisciplines = res
        this.show = false
      }, error => {
        console.error('Erro ao buscar disciplinas: ' + JSON.stringify(error))
      })
  }

  processFile(image) {
    const file: File = image.files[0]
    const reader: FileReader = new FileReader()
    reader.onloadend = (e) => {
      this.imageUser = reader.result
      const payload = { img: this.imageUser }
      this.rest.updateApi('usuario', payload, this.user.cdUsuario)
        .pipe(takeUntil(this.destroyed$)).subscribe(
          res => {
            localStorage.setItem('currentUser', JSON.stringify(res))
            location.reload()
          }, error => {
            console.error('Error no update da imagem: ' + error)
          })
    }
    reader.readAsDataURL(file)
  }


  generateTags() {
    // default values
    const config = {
      title : this.user.nome,
      description: 'Veja meu perfil e publicações no Portfólio Unisinos.',
      image : this.dService.logoUnisinos()
    }

    this.meta.updateTag({ name: 'twitter:card', content: 'summary' })
    this.meta.updateTag({ name: 'twitter:site', content: '@UnisinosPortfolio' })
    this.meta.updateTag({ name: 'twitter:title', content: config.title })
    this.meta.updateTag({ name: 'twitter:description', content: config.description })
    this.meta.updateTag({ name: 'twitter:image', content: config.image })

    this.meta.updateTag({ property: 'og:type', content: 'article' })
    this.meta.updateTag({ property: 'og:site_name', content: 'Unisinos Portfolio' })
    this.meta.updateTag({ property: 'og:title', content: config.title })
    this.meta.updateTag({ property: 'og:description', content: config.description })
    this.meta.updateTag({ property: 'og:image', content: config.image })
    this.meta.updateTag({ property: 'og:url', content: window.location.href})
  }


  shareClick() {
    const dialogConfig = new MatDialogConfig()
    dialogConfig.data = this.user
    dialogConfig.disableClose = true
    dialogConfig.autoFocus = false
    const dialogRef = this.dialog.open(ModalShareComponent, dialogConfig)
    dialogRef.componentInstance.url = dialogRef.componentInstance.url = window.location.href.replace('meu-perfil', '') + this.user.url + this.user.url
    dialogRef.componentInstance.desc = this.user.nome
  }

  ngOnDestroy() {
    this.destroyed$.next(true)
    this.destroyed$.complete()
  }
}
