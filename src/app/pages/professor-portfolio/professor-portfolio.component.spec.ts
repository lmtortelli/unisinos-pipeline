import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfessorPortfolioComponent } from './professor-portfolio.component';

describe('ProfessorPortfolioComponent', () => {
  let component: ProfessorPortfolioComponent;
  let fixture: ComponentFixture<ProfessorPortfolioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfessorPortfolioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfessorPortfolioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
