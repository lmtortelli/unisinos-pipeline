import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core'
import { Router } from '@angular/router'
import { RestapiService } from 'src/app/services/restapi.service'
import { DefaultService } from 'src/app/services/global/default.service'
import { UsuarioModel } from 'src/app/models/usuario'
import { statusPublication, OrderSelect } from 'src/app/app.component'
import { PublicationModel } from 'src/app/models/publication-model'
import { AppState } from 'src/app/redux/app.state'
import { State } from '@ngrx/store'
import { Store } from '@ngrx/store'
import { ReplaySubject } from 'rxjs'
import { takeUntil } from 'rxjs/operators'
import { PortfolioModel } from 'src/app/models/portfolio.model'


@Component({
  selector: 'app-professor-portfolio',
  templateUrl: './professor-portfolio.component.html',
  styleUrls: ['./professor-portfolio.component.scss']
})
export class ProfessorPortfolioComponent implements OnInit {
  panelOpenState: boolean
  // Page
  show: boolean
  private user: UsuarioModel = <any>{}

  // Publicacoes
  private publications: any = []
  private offset = 0
  private previousLength = 0 // Aux for button More Result
  private idCoursesPublications: any = new Set()
  private users: any[]

  // Portfolios
  private offsetPortfolio = 0
  private portfolios: any = []
  private previousLengthPortfolios = 0

  // FLAGS
  // Publicacoes
  hasPublications = false
  hasFilters = false
  private mensageFilterEmpty = false
  visibleMorePublication: boolean
  // Portfolios
  hasFiltersPortfolio = false
  private mensageFilterEmptyPortfolio = false
  visibleMorePortfolios: boolean
  hasPortfolios = false

  // Enum
  private statusPublication = `${statusPublication}`
  private orderSelect = `${OrderSelect}`
  private optionSelected = OrderSelect.Recent
  private optionPortfolio = OrderSelect.AlfaOrder

  // Combos
  private selects: any = {
    period: {},
    disciplines: {},
    tests: {},
    status: {},
    courses: {}
  }

  private filters: any = {
    periodSelected: 'Todos',
    disciplineSelected: 'Todas',
    courseSelected: 'Todos',
    nameUser: 'Digite o nome ...'
  }

  // Loader Control
  private initial = true
  private initialLoader = {
    portfolioFinish: false,
    publicationFinish: false
  }

  private logMap = new Map()
  private portfolioMap = new Map()

  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1)

  constructor(private rest: RestapiService,
    public dService: DefaultService,
    private router: Router,
    private state: State<AppState>,
    private store: Store<AppState>) {
    this.visibleMorePublication = false
    this.visibleMorePortfolios = false
  }

  loader() {
    this.show = !this.show
  }


  checkFinishLoader() {
    if (this.initialLoader.portfolioFinish && this.initialLoader.publicationFinish) {
      this.show = false
    }
  }

  async ngOnInit() {
    this.show = true
    this.logMap.clear()
    this.portfolioMap.clear()
    await this.state.pipe(takeUntil(this.destroyed$)).subscribe(async appState => {
      if (appState.auth.token != null) {
        this.user = await appState.auth.user.usuario

        const flgProf = localStorage.getItem('professor_flag')

        if (this.user.url == null) {
          this.router.navigateByUrl('/cadastro')
        }

        this.loader()
        this.getPeriods()
        this.getDisciplines()
        this.getCoursesPublications()
        this.getStudentsPublications()
        this.getPortfolios()
      } else {
        this.router.navigateByUrl('/home')
      }
    })

  }

  //#region "Student"
  getPeriods() {
    this.rest.getByIdApi('periodo/professor', this.user.idUsuario)
      .pipe(takeUntil(this.destroyed$)).subscribe(res => {
        this.selects.periods = res
      }, error => {
        this.loader()
        console.error('Erro ao buscar periodos: ' + JSON.stringify(error))
      })
  }

  getDisciplines(payload = {}) {
    this.rest.postApi('disciplina/professor/' + this.user.idUsuario, payload)
      .pipe(takeUntil(this.destroyed$)).subscribe(res => {
        this.selects.disciplines = res
      }, error => {
        this.loader()
        console.error('Erro ao buscar disciplinas: ' + JSON.stringify(error))
      })
  }

  buildPayload(): any {
    const payload = {
      idPeriodoLetivo: this.filters.periodSelected.idPeriodoLetivo,
      idDisciplina: this.filters.disciplineSelected.idDisciplina,
      idCurso: this.filters.courseSelected.idCurso,
      nameUser: this.filters.nameUser
    }

    if (this.filters.periodSelected == 'Todos') {
      delete payload.idPeriodoLetivo
    }
    if (this.filters.nameUser == 'Digite o nome ...') {
      delete payload.nameUser
    }
    if (this.filters.courseSelected == 'Todos') {
      delete payload.idCurso
    }
    if (this.filters.disciplineSelected == 'Todas') {
      delete payload.idDisciplina
    }
    return payload
  }

  buildPayloadPortfolio(): any {
    const payload = {
      idPeriodoLetivo: this.filters.periodSelected.idPeriodoLetivo,
      idDisciplina: this.filters.disciplineSelected.idDisciplina,
      idCurso: this.filters.courseSelected.idCurso
    }

    if (this.filters.periodSelected == 'Todos') {
      delete payload.idPeriodoLetivo
    }
    if (this.filters.courseSelected == 'Todos') {
      delete payload.idCurso
    }
    if (this.filters.disciplineSelected == 'Todas') {
      delete payload.idDisciplina
    }
    return payload
  }


  getNamesByFiltro() {
    const payload = this.buildPayload()
    this.rest.postApi('usuario/professor/filtro/', payload)
      .pipe(takeUntil(this.destroyed$)).subscribe(res => {
        this.users = res
        this.users.sort((a, b) => a.nome.localeCompare(b.nome))
      }, error => {
        console.error('Erro ao buscar publicações: ' + JSON.stringify(error))
      })
  }

  submitFilters() {
    this.show = true
    this.offset = 0
    this.logMap.clear()
    this.idCoursesPublications.clear()
    this.getStudentsPublications(true)

  }

  submitFiltersPortfolios() {
    this.show = true
    this.offsetPortfolio = 0
    this.portfolioMap.clear()
    this.getPortfolios()
  }

  getPortfolios() {
    const payload = this.buildPayloadPortfolio()
    const isFilter = Object.keys(payload).length
    payload['idUsuarioProfessor'] = this.user.idUsuario
    this.rest.postApi('usuario/portfolio/' + this.offsetPortfolio, payload)
      .pipe(takeUntil(this.destroyed$)).subscribe(res => {
        if (res != null && res.length >= 1) {
          this.visibleMorePortfolios = true
          res.forEach(element => {
            this.mensageFilterEmptyPortfolio = false
            this.hasPortfolios = true
            this.hasFiltersPortfolio = true
            const port = <PortfolioModel>element
            if (!this.portfolioMap.has(port.idUsuario)) {
              this.portfolioMap.set(port.idUsuario, port)
            }

          })
        } else {
          this.show = false
          this.visibleMorePortfolios = false
          if (this.portfolioMap.size == 0) {
            if (isFilter != 0) {
              this.mensageFilterEmptyPortfolio = true
            }

            this.hasPortfolios = false
            // this.hasFiltersPortfolio = false
          }
        }
        this.onChangeOrderPortfolio(this.optionPortfolio)
        this.show = false
      }, error => {
        this.show = false
        console.error('Erro ao buscar publicações: ' + JSON.stringify(error))
      })
  }

  UpdatePublications(res: any, isFilter: number, update = false) {
    if (res !== null && res.length > 0) {
        this.visibleMorePublication = true
        res.forEach(element => {
          if (element.cdStatus != statusPublication.Draft) {
            this.mensageFilterEmpty = false
            this.hasPublications = true
            const publ = <PublicationModel>element
            if (!this.logMap.has(publ.idPublicacao)) {
              this.hasFilters = true
              this.logMap.set(publ.idPublicacao, publ)
            }
          } else {
            this.visibleMorePublication = false
      if (this.logMap.size == 0) {
        if (isFilter != 0) {
          this.mensageFilterEmpty = true
        }
        this.hasPublications = false
      }
          }
        })
        this.onChangeOrder(this.optionSelected)
    } else {
      this.visibleMorePublication = false
      if (this.logMap.size == 0) {
        if (isFilter != 0) {
          this.mensageFilterEmpty = true
        }
        this.hasPublications = false
      }
    }
    if (update) {
      this.show = false
    }
  }

  onChangePeriods(event) {
    const payload = { idPeriodoLetivo: event.idPeriodoLetivo }
    this.getDisciplines(payload)
    this.getCoursesPublications(payload)
    this.filters.nameUser = 'Digite o nome ...'
    this.filters.disciplineSelected = 'Todas'
    this.getNamesByFiltro()

  }

  onChangeCurso(event) {
    this.filters.courseSelected = event
    const payload = {
      idCurso: event.idCurso,
      idPeriodoLetivo: this.filters.periodSelected.idPeriodoLetivo
    }
    this.getDisciplines(payload)
    this.filters.nameUser = 'Digite o nome ...'
    this.filters.disciplineSelected = 'Todas'
    this.getNamesByFiltro()
  }

  onChangeDisciplines(event) {
    const payload = { idDisciplina: event.idDisciplina }
    this.filters.nameUser = 'Digite o nome ...'
    this.getNamesByFiltro()
  }

  clearText() {
    this.filters.nameUser = ''
  }

  onChangeOrder(event) {
    if (this.publications.length !== this.logMap.size) {
      this.publications = []
      this.logMap.forEach(val => {
        this.publications.push(val)
      })
    }

    switch (event) {
      case OrderSelect.Recent: {

        this.publications.sort((a, b) => (a.dtCadastro < b.dtCadastro) ? 1 : -1)
        break
      }
      case OrderSelect.Old: {
        this.publications.sort((a, b) => (a.dtCadastro > b.dtCadastro) ? 1 : -1)
        break
      }
      case OrderSelect.AlfaOrder: {
        this.publications.sort((a, b) => a.titulo.localeCompare(b.titulo))
        break
      }
      case OrderSelect.AlfaInverseOorder: {
        this.publications.sort((a, b) => b.titulo.localeCompare(a.titulo))
        break
      }
    }
  }

  onChangeOrderPortfolio(event) {
    if (this.portfolios.length !== this.portfolioMap.size) {
      this.portfolios = []
      this.portfolioMap.forEach(val => {
        this.portfolios.push(val)
      })
    }
    switch (event) {
      case OrderSelect.AlfaOrder: {
        this.portfolios.sort((a, b) => a.nome.localeCompare(b.nome))
        break
      }
      case OrderSelect.AlfaInverseOorder: {
        this.portfolios.sort((a, b) => b.nome.localeCompare(a.nome))
        break
      }
    }
  }

  UpdateMorePublication() {
    this.show = true
    this.offset += 8
    this.getStudentsPublications(true)
  }

  UpdateMorePortfolios() {
    this.show = true
    this.offsetPortfolio += 16
    this.getPortfolios()
  }


  //#region "Professor"
  getStudentsPublications(update = false) {
    const payload = this.buildPayload()
    const isFilter = Object.keys(payload).length
    this.rest.postApi('publicacao/filtro/' + this.user.idUsuario + '/' + this.offset, payload)
      .pipe(takeUntil(this.destroyed$)).subscribe(res => {
        this.UpdatePublications(res, isFilter, update)

      }, error => {
        this.loader()
        console.error('Erro ao buscar publicações: ' + JSON.stringify(error))
      })
  }

  getCoursesPublications(payload = {}) {
    // curso/professor/
    payload['idUsuario'] = this.user.idUsuario
    this.rest.postApi('curso/professor/', payload)
      .pipe(takeUntil(this.destroyed$)).subscribe(res => {
        this.selects.courses = res
      }, error => {
        console.error('Erro ao buscar disciplinas: ' + JSON.stringify(error))
      })
  }
  //#endregion "Professor"
  ngOnDestroy() {
    this.destroyed$.next(true)
    this.destroyed$.complete()
  }
}
