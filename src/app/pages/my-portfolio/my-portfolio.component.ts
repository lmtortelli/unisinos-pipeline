import { Component, OnInit, Input, ViewEncapsulation, ViewChild } from '@angular/core'
import { MatDialogConfig, MatDialog } from '@angular/material'
import { Router } from '@angular/router'
import { RestapiService } from 'src/app/services/restapi.service'
import { DefaultService } from 'src/app/services/global/default.service'
import { ModalInitialPrivacyComponent } from 'src/app/component/modal-initial-privacy/modal-initial-privacy.component'
import { UsuarioModel } from 'src/app/models/usuario'
import { statusPublication, OrderSelect } from 'src/app/app.component'
import { PublicationModel } from 'src/app/models/publication-model'
import { AppState } from 'src/app/redux/app.state'
import { State } from '@ngrx/store'
import { Store } from '@ngrx/store'
import { ACTIVE_MENU } from 'src/app/redux/constants'
import { ModalAtividadeAcademicaComponent } from 'src/app/component/modal-atividade-academica/modal-atividade-academica.component'
import { ScrollStrategyOptions } from '@angular/cdk/overlay'
import { ModalShareComponent } from 'src/app/component/modal-share/modal-share.component'
import { ReplaySubject } from 'rxjs'
import { takeUntil } from 'rxjs/operators'
import { Meta } from '@angular/platform-browser'

@Component({
  selector: 'app-my-portfolio',
  templateUrl: './my-portfolio.component.html',
  styleUrls: ['./my-portfolio.component.scss']
})

export class MyPortfolioComponent implements OnInit {
  panelOpenState: boolean
  publications: any = []
  publicationsDrafts: any = []
  user: UsuarioModel = <any>{}
  firstTime = false
  imageUser: string | ArrayBuffer
  courses: string
  hasPublications = false
  hasDrafts = false
  hasFilters = false
  statusPublication = `${statusPublication}`
  selects: any = {
    period: {},
    disciplines: {},
    tests: {},
    status: {},
    course: {}
  }
  offset = 0
  offsetTimeline = 10
  offsetHasDraft = 0
  filters: any = {
    periodSelected: 'Todos',
    disciplineSelected: 'Todas',
    testSelected: 'Todas',
    statusSelected: 'Todos',
    courseSelect: 'Todos',
    nameUser: 'Digite o nome ...'
  }

  timelineFilters: any = {
    periodSelected: 'Todos',
    disciplineSelected: 'Todas',
    testSelected: 'Todas'
  }
  activies = true

  template: any = {
    linkedin: 'https://www.linkedin.com/in/',
    twitter: 'https://twitter.com/',
    facebook: 'https://www.facebook.com/',
    lattes: 'http://lattes.cnpq.br/'
  }

  visibleMorePublication: boolean
  visibleMorePublicationDrafts: boolean
  orderSelect = `${OrderSelect}`
  optionSelected = OrderSelect.Recent
  optionSelectedTimeline = OrderSelect.Recent
  mensageFilterEmpty = false
  show: boolean
  @Input() urlParcial: string
  users: any[]

  coursesSet: any = new Set()
  previousLength = 0
  previousLengthDraft = 0
  // Professor


  @ViewChild('timeline')
  private timeline: any
  btnTimeline = true

  // Loader Control
  private initial = true
  private initialLoader = {
    draftFinish: false,
    publicationFinish: false,
    timelineFinish: false
  }
  private logMap = new Map()
  private logMapDrafts = new Map()

  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1)

  constructor(private rest: RestapiService,
    public dService: DefaultService,
    public dialog: MatDialog,
    private router: Router,
    private meta: Meta,
    private state: State<AppState>,
    private store: Store<AppState>) {
    this.courses = ''
    this.visibleMorePublicationDrafts = false
    this.visibleMorePublication = false

  }

  loader() {
    this.show = !this.show
  }

  checkFinishLoader() {
    if (this.initialLoader.draftFinish &&
      this.initialLoader.publicationFinish && this.initialLoader.timelineFinish) {
      this.show = false
    }
  }

  async ngOnInit() {
    this.logMap.clear()
    this.show = true
    await this.state.pipe(takeUntil(this.destroyed$)).subscribe(async appState => {
      if (appState.auth.token != null) {
        this.user = await appState.auth.user.usuario
        if (this.user.url == null) {
          this.router.navigateByUrl('/cadastro')
        }
        if (this.user.img == null) {
          this.imageUser = this.dService.standardUserImage()
        } else {
          this.imageUser = this.user.img
        }

        if (localStorage.getItem('professor_flag') == 'true') {
          this.router.navigateByUrl('/meus-alunos')
        }

        this.getPeriods()
        this.getDisciplines()

        if (localStorage.getItem('firstTime')) {
          this.show = false
          setTimeout(() => this.openModal())
          this.loader()
        }
        this.getCourse()
        this.getTests()
        this.getPublications()
        this.getPublicationsHasDraft()
      } else {
        this.router.navigateByUrl('/home')
      }
    })
  }

  //#region "Student"
  getCourse() {
    this.rest.getByIdApi('usuario/curso', this.user.cdUsuario).pipe(takeUntil(this.destroyed$)).subscribe(res => {
      if (!res.cursos) {
        // this.show = false
        return
      }
      res.cursos.forEach(element => {
        if (this.courses === '') {
          this.coursesSet.add(element.nome)
        } else {
          this.coursesSet.add(element.nome)
        }
        this.courses = ''
        let space = ''
        if (this.coursesSet.size > 1) {
          space = ' - '
        }

        this.coursesSet.forEach(el => {
          this.courses = this.courses + el + space
        })
      })
    }, error => {
      this.loader()
      console.error('Erro ao buscar o curso: ' + error)
    })
  }

  getPeriods() {

    this.rest.getByIdApi('periodo/usuario', this.user.cdUsuario).pipe(takeUntil(this.destroyed$)).subscribe(res => {
      this.selects.periods = res
    }, error => {
      console.error('Erro ao buscar periodos: ' + JSON.stringify(error))
    })

  }

  getDisciplines(payload = {}) {
    this.rest.postApi('disciplina/' + this.user.cdUsuario, payload).pipe(takeUntil(this.destroyed$)).subscribe(res => {
      this.filters.disciplineSelected = 'Todas'
      this.selects.disciplines = res
    }, error => {
      console.error('Erro ao buscar disciplinas: ' + JSON.stringify(error))
    })
  }

  buildPayload(): any {
    const payload = {
      idPeriodoLetivo: this.filters.periodSelected.idPeriodoLetivo,
      idDisciplina: this.filters.disciplineSelected.idDisciplina,
      idCurso: this.filters.courseSelect.idCurso,
      idAvaliacao: this.filters.testSelected.idAvaliacao,
      cdStatus: this.filters.statusSelected,
      nameUser: this.filters.nameUser
    }


    if (this.filters.periodSelected == 'Todos') {
      delete payload.idPeriodoLetivo
    }
    if (this.filters.nameUser == 'Digite o nome ...') {
      delete payload.nameUser
    }
    if (this.filters.courseSelect == 'Todos') {
      delete payload.idCurso
    }
    if (this.filters.disciplineSelected == 'Todas') {
      delete payload.idDisciplina
    }
    if (this.filters.testSelected == 'Todas') {
      delete payload.idAvaliacao
    }
    if (this.filters.statusSelected == 'Todos') {
      delete payload.cdStatus
    }

    return payload
  }

  resetChild() {
    this.offsetTimeline = 10
    this.btnTimeline = true
    this.timeline.ngOnInit()
  }

  showSpinner(act: boolean) {
    this.show = act
  }

  getTests(payload = {}) {
    this.rest.postApi('avaliacao/' + this.user.cdUsuario, payload).pipe(takeUntil(this.destroyed$)).subscribe(res => {
      this.filters.testSelected = 'Todas'
      this.selects.tests = res
    }, error => {
      this.show = false
      console.error('Erro ao buscar avaliações: ' + JSON.stringify(error))
    })
  }

  moreTimeline(event: boolean) {
    this.btnTimeline = event
  }


  submitFilters() {
    this.loader()
    const payload = this.buildPayload()
    this.offset = 0
    this.logMap.clear()

    this.rest.postApi('usuario/filtro/' + this.user.cdUsuario + '/' + this.offset , payload).pipe(takeUntil(this.destroyed$)).subscribe(res => {
      this.UpdatePublicationsByFilter(res, true)
    }, error => {
      console.error('Erro ao buscar publicações: ' + JSON.stringify(error))
    })


  }

  UpdatePublicationsByFilter(res: any, update = false) {
    if (res !== null && res.length > 0) {
      res.forEach(element => {
        if (element.cdStatus != statusPublication.Draft) {
          this.visibleMorePublication = true
          this.mensageFilterEmpty = false
          this.hasPublications = true
          const publ = <PublicationModel>element
          if (!this.logMap.has(publ.idPublicacao)) {
            this.hasFilters = true
            this.logMap.set(publ.idPublicacao, publ)
          }
        } else {
          this.visibleMorePublication = false
          if (this.logMap.size === 0) {
            this.mensageFilterEmpty = update
            this.hasPublications = false
          }
        }
      })
      this.onChangeOrder(this.optionSelected)
    } else {
      this.visibleMorePublication = false
          if (this.logMap.size == 0) {
            this.mensageFilterEmpty = update
            this.hasPublications = false
          }
    }

    this.show = false
  }

  onChangePeriods(event) {
    const payload = { idPeriodoLetivo: event.idPeriodoLetivo }
    this.getDisciplines(payload)
  }

  onChangeCurso() {
    const payload = {
      idCurso: this.filters.courseSelect.idCurso,
      idPeriodoLetivo: this.filters.periodSelected.idPeriodoLetivo
    }
    this.getDisciplines(payload)
  }

  onChangeDisciplines(event) {
    const payload = { idDisciplina: event.idDisciplina }
    this.getTests(payload)
  }

  clearText() {
    this.filters.nameUser = ''
  }

  openDialog(): void {
    const dialogConfig = new MatDialogConfig()
    dialogConfig.disableClose = true
    dialogConfig.autoFocus = true
    const dialogRef = this.dialog.open(ModalAtividadeAcademicaComponent, dialogConfig)
    dialogRef.componentInstance.user = this.user
  }

  onChangeOrder(event) {
    if (this.publications.length !== this.logMap.size) {
      this.publications = []
      this.logMap.forEach(val => {
        this.publications.push(val)
      })
    }
    this.publicationsDrafts = []
    this.logMapDrafts.forEach(val => {
      this.publicationsDrafts.push(val)
    })


    switch (event) {
      case OrderSelect.Recent: {
        this.publications.sort((a, b) => (a.dtCadastro < b.dtCadastro) ? 1 : -1)
        break
      }
      case OrderSelect.Old: {
        this.publications.sort((a, b) => (a.dtCadastro > b.dtCadastro) ? 1 : -1)
        break
      }
      case OrderSelect.AlfaOrder: {
        this.publications.sort((a, b) => a.titulo.localeCompare(b.titulo))
        break
      }
      case OrderSelect.AlfaInverseOorder: {
        this.publications.sort((a, b) => b.titulo.localeCompare(a.titulo))
        break
      }
    }
  }

  processFile(image) {
    const file: File = image.files[0]
    const reader: FileReader = new FileReader()

    reader.onloadend = (e) => {
      this.imageUser = reader.result
      const payload = { img: this.imageUser }

      this.rest.updateApi('usuario', payload, this.user.cdUsuario).pipe(takeUntil(this.destroyed$)).subscribe(
        res => {
          localStorage.setItem('currentUser', JSON.stringify(res))
          location.reload()
        }, error => {
          console.error('Error no update da imagem: ' + error)
        }
      )
    }
    reader.readAsDataURL(file)
  }

  openModal() {
    const dialogConfig = new MatDialogConfig()
    dialogConfig.disableClose = true
    dialogConfig.autoFocus = true
    const dialogRef = this.dialog.open(ModalInitialPrivacyComponent, dialogConfig)
    dialogRef.afterClosed().subscribe(result => {
      localStorage.removeItem('firstTime')
    })
  }

  getPublications() {
    const payload = this.buildPayload()
    this.rest.postApi('usuario/filtro/' + this.user.cdUsuario + '/' + this.offset, payload).pipe(takeUntil(this.destroyed$)).subscribe(
      res => {
        this.UpdatePublicationsByFilter(res, false)
      }, error => {
        this.loader()
        console.error('Erro ao buscar publicações: ' + JSON.stringify(error))
      })
  }

  getPublicationsHasDraft() {
    this.rest.getByIdApi('usuario/publicacao/rascunho', this.user.cdUsuario + '/' + this.offsetHasDraft)
      .pipe(takeUntil(this.destroyed$)).subscribe(res => {
        if (res.publicacao.length > 0) {
          this.visibleMorePublicationDrafts = true
          res.publicacao.forEach(element => {
            if (element.cdStatus == statusPublication.Draft) {
              const publ = <PublicationModel>element
              if (!this.logMapDrafts.has(publ.idPublicacao)) {
                this.hasDrafts = true
                this.logMapDrafts.set(publ.idPublicacao, publ)
              }
            }
          })
        } else {
          this.visibleMorePublicationDrafts = false
        }
        if (this.initial) {
          this.initialLoader.draftFinish = true
          this.checkFinishLoader()
        } else {
          this.show = false
        }
      }, error => {
        this.loader()
        console.error('Erro ao buscar publicações: ' + JSON.stringify(error))
      })

  }

  UpdateMorePublication() {
    this.show = true
    this.offset += 8
    this.getPublications()

  }

  UpdateMorePublicationHasDraft() {
    this.offsetHasDraft += 8
    this.getPublicationsHasDraft()
  }


  UpdateMoreActivies() {
    this.loader()
    this.offsetTimeline += 10
  }

  shareClick() {
    const dialogConfig = new MatDialogConfig()
    dialogConfig.data = this.user
    dialogConfig.disableClose = true
    dialogConfig.autoFocus = false
    const dialogRef = this.dialog.open(ModalShareComponent, dialogConfig)
    dialogRef.componentInstance.url = window.location.href.replace('meu-portfolio', '') + this.user.url
    dialogRef.componentInstance.desc = this.user.nome
  }

  generateTags() {
    // default values
    const config = {
      title : this.user.nome,
      description: 'Veja meu perfil e publicações no Portfólio Unisinos.',
      image : this.dService.logoUnisinos()
    }

    this.meta.updateTag({ name: 'twitter:card', content: 'summary' })
    this.meta.updateTag({ name: 'twitter:site', content: '@UnisinosPortfolio' })
    this.meta.updateTag({ name: 'twitter:title', content: config.title })
    this.meta.updateTag({ name: 'twitter:description', content: config.description })
    this.meta.updateTag({ name: 'twitter:image', content: config.image })

    this.meta.updateTag({ property: 'og:type', content: 'article' })
    this.meta.updateTag({ property: 'og:site_name', content: 'Unisinos Portfolio' })
    this.meta.updateTag({ property: 'og:title', content: config.title })
    this.meta.updateTag({ property: 'og:description', content: config.description })
    this.meta.updateTag({ property: 'og:image', content: config.image })
    this.meta.updateTag({ property: 'og:url', content: window.location.href})
  }

  //#endregion "Student"

  hasActivies(act: boolean) {
    this.activies = act
  }
  //#endregion "Professor"
  ngOnDestroy() {
    this.destroyed$.next(true)
    this.destroyed$.complete()
    this.dialog.closeAll()
    this.loader()
  }
}
