import { Injectable } from '@angular/core'

import { RestapiService } from '../restapi.service'
import { Usuario } from 'src/app/models/usuario'

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  _userInfo: Usuario = <any>{}

  constructor(private api: RestapiService) { }

  set userInfo(value: any) {
    this._userInfo = new Usuario(value)
  }

  get userInfo(): any {
    return this._userInfo
  }

  getLoggedUser = () =>
    new Promise((resolve, reject) =>
      this.api.getApi('usuario/get/me')
        .subscribe(
          data => resolve(data),
          err => reject(err)
        )
    )
}
