import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs'
import { map, catchError, tap } from 'rxjs/operators'
import { AppState } from '../redux/app.state'
import { State } from '@ngrx/store'

@Injectable({
  providedIn: 'root'
})

export class RestapiService {
  modelError: Array<any>
  url = 'http://localhost:8080/api/'
  // url = 'http://35.174.137.99:8080/api/'
  // url = 'http://191.4.142.172:8080/api/'
  token: string
  httpOptions: object

  constructor(
    private http: HttpClient,
    private state: State<AppState>
  ) {
    this.state.subscribe(appState => {
      const { auth } = appState
      if (auth && auth.token) {
        this.token = auth.token
        this.httpOptions = {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.token
          })
        }
      }
    })
  }

  private extractData(res: Response) {
    const body = res

    return body || {}
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (err: any): any => {
      this.modelError = [{ err: err.error }]
      // TODO: send the error to remote logging infrastructure
      console.error(err) // log to console instead
      if (err.status == 401) {
        console.error(this.modelError)
        return this.modelError
      } else {
        console.error(err.status) // log to console instead
        // TODO: better job of transforming error for user consumption
        console.log(`${operation} failed: ${err.message}`)
        console.error(this.modelError)
        return this.modelError
      }
    }
  }

  getApi(path): Observable<any> {
    return this.http.get(this.url + path, this.httpOptions).pipe(
      map(this.extractData))
  }

  getByIdApi(path, param): Observable<any> {
    return this.http.get(this.url + path + '/' + param, this.httpOptions).pipe(
      map(this.extractData))
  }

  getByApi(path, param, value): Observable<any> {
    return this.http.get(this.url + path + '/' + param + '/' + value, this.httpOptions).pipe(
      map(this.extractData))
  }

  postApi(path, payload): Observable<any> {
    return this.http.post<any>(this.url + path, payload, this.httpOptions).pipe(
      tap((model) => {}),
      catchError(this.handleError<any>('Operation: add ' + path))
    )
  }

  updateApi(path, model, param): Observable<any> {
    return this.http.put(this.url + path + '/' + param, JSON.stringify(model), this.httpOptions).pipe(
      tap(_ => {}),
      catchError(this.handleError<any>('Operation: update ' + path))
    )
  }

  deleteApi(path, param): Observable<any> {
    return this.http.delete<any>(this.url + path + '/' + param, this.httpOptions).pipe(
      tap(_ => {}),
      catchError(this.handleError<any>(''))
    )
  }
}
