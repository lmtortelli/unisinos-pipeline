import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor() {
  }
  isLoggedIn() {
    if (localStorage.getItem('currentUser') != 'undefined' && localStorage.getItem('currentUser') != null) {
      return true;
    } else {
      return false;
    }
  }
}
