import { Injectable } from '@angular/core'
import { RestapiService } from '../restapi.service'
import { Observable } from 'rxjs'
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class UploadServiceService {

  constructor(private restApi: RestapiService, private http: HttpClient) { }

  uploadFileObs(): Observable<any> {
    return
  }

  uploadFile(data) {
    console.log(JSON.stringify(data))
    return this.http.post<any>(`${this.restApi.url}upload`, data)
  }
}
