import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { ModalConfirmationComponent } from 'src/app/component/modal-confirmation/modal-confirmation.component'
import { MatDialogConfig, MatDialog } from '@angular/material'

@Injectable({
  providedIn: 'root'
})
export class DefaultService {


  constructor(
    private dialog: MatDialog
  ) { }

  standardUserImage() {
    return 'assets/img/perfil.png'
  }

  logoUnisinos() {
    return 'assets/img/logo-share.png'
  }

  confimationModal(): Observable<boolean> {
    return new Observable(observer => {
      const dialogConfig = new MatDialogConfig()
      dialogConfig.data = {
        areRemove : false
      }
      dialogConfig.disableClose = true
      dialogConfig.autoFocus = true
      const dialogRef = this.dialog.open(ModalConfirmationComponent, dialogConfig)
      dialogRef.afterClosed().subscribe(result => {
          observer.next(dialogRef.componentInstance.areRemove)
          observer.complete()
      })
    })
  }
}
