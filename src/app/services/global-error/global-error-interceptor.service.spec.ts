import { TestBed } from '@angular/core/testing';

import { GlobalErrorInterceptorService } from './global-error-interceptor.service';

describe('GlobalErrorInterceptorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GlobalErrorInterceptorService = TestBed.get(GlobalErrorInterceptorService);
    expect(service).toBeTruthy();
  });
});
