import { Injectable } from '@angular/core'
import { MatDialogConfig, MatDialog } from '@angular/material'
import { ModalAlertComponent } from 'src/app/component/modal-alert/modal-alert.component'

@Injectable({
  providedIn: 'root'
})
export class GlobalErrorService {

  constructor(
    public dialog: MatDialog) { }


  open() {
    const dialogConfig = new MatDialogConfig()
    dialogConfig.disableClose = true
    dialogConfig.autoFocus = true
    this.dialog.open(ModalAlertComponent, dialogConfig)
  }
}
