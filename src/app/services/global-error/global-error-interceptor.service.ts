import { Injectable, Injector } from '@angular/core'
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http'
import { Observable, pipe, throwError } from 'rxjs'
import { tap, catchError } from 'rxjs/operators'
import { GlobalErrorService } from './global-error.service'

  // TODO: Log Service for all errors


@Injectable({
  providedIn: 'root'
})
export class GlobalErrorInterceptorService implements HttpInterceptor {
  constructor(private globalService: GlobalErrorService) { }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {



    return next.handle(req).pipe(
      catchError((error: HttpErrorResponse) => {
        let testUserMe = false
        if (error.url !== null) {
          testUserMe = error.url.includes('usuario/me')
        }

        if (!testUserMe) {
          if (error.status != 401) {
            this.globalService.open()
          }
        }
        return throwError(error)
      }))
  }
}
