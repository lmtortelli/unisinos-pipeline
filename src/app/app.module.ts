import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClientJsonpModule } from '@angular/common/http'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { MatTabsModule } from '@angular/material/tabs'
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner'
import { MatDialogModule, MatExpansionModule, MatFormFieldModule, MatInputModule, MatSnackBarModule, MatSidenavModule } from '@angular/material'
import { MatButtonModule } from '@angular/material'
import { MatSelectModule } from '@angular/material/select'
import { MatCardModule } from '@angular/material/card'
import { MatTooltipModule } from '@angular/material/tooltip'
import { DatePipe } from '@angular/common'
import { StoreModule } from '@ngrx/store'
import { MediumEditorModule } from 'angular2-medium-editor'
import { DragDropModule } from '@angular/cdk/drag-drop'
import { EmbedVideo } from 'ngx-embed-video'
import { SafePipeModule } from 'safe-pipe'
import {MatListModule} from '@angular/material/list'
import {MatStepperModule} from '@angular/material/stepper'
import { ShareModule } from '@ngx-share/core';

import { AppComponent } from './app.component'
import { LandingComponent } from './pages/landing/landing.component'
import { AppRoutingModule } from './app-routing.module'
import { HeaderComponent } from './component/header/header.component'
import { FooterComponent } from './component/footer/footer.component'
import { UploadComponent } from './pages/upload/upload.component'
import { PopoverLoginComponent } from './component/popover-login/popover-login.component'
import { RegisterProfileComponent } from './pages/register-profile/register-profile.component'
import { MyPortfolioComponent } from './pages/my-portfolio/my-portfolio.component'
import { HeaderPortfolioComponent } from './component/header-portfolio/header-portfolio.component'
import { ModalConfigUserComponent } from './component/modal-config-user/modal-config-user.component'
import { ModalInitialPrivacyComponent } from './component/modal-initial-privacy/modal-initial-privacy.component'
import { NavMenuUserComponent } from './component/nav-menu-user/nav-menu-user.component'
import { NewPublicationComponent } from './pages/new-publication/new-publication.component'
import { ModalConfigPostComponent } from './component/modal-config-post/modal-config-post.component'
import { PublicationsComponent } from './component/publications/publications.component'
import { PublicationComponent } from './component/publications/publication/publication.component'
import { LoaderComponent } from './component/loader/loader.component'
import { LoaderInterceptorService } from './services/loader/loader-interceptor.service'
import * as reducers from './redux/reducers'
import { ModalSetPortfolioComponent } from './component/modal-set-portfolio/modal-set-portfolio.component'
import { TextPostComponent } from './component/text-post/text-post.component'
import { ModalAlertComponent } from './component/modal-alert/modal-alert.component'
import { GlobalErrorInterceptorService } from './services/global-error/global-error-interceptor.service'
import { ModalEmbedComponent } from './component/modal-embed/modal-embed.component'
import { PublicationDetailComponent } from './pages/publication-detail/publication-detail.component'
import { UserInfoPublicationComponent } from './component/user-info-publication/user-info-publication.component'
import { SocialSectionComponent } from './component/social-section/social-section.component'
import { FeedbackSectionComponent } from './component/social-section/feedback-section/feedback-section.component'
import { CommentsComponent } from './component/social-section/comments/comments.component'
import { CommentComponent } from './component/social-section/comments/comment/comment.component'
import { FeedbackComponent } from './component/social-section/feedback-section/feedback/feedback.component'
import { FeedbackInputComponent } from './component/social-section/feedback-section/feedback-input/feedback-input.component'
import { ReflectionUserComponent } from './component/social-section/feedback-section/reflection-user/reflection-user.component'
import { SanitizeHtmlPipe } from './pipe/sanitizeHtml'
import { ProfileComponent } from './pages/profile/profile.component'
import { TimelineComponent } from './component/timeline/timeline.component'
import { ActivityComponent } from './component/timeline/activity/activity.component'
import { ModalAtividadeAcademicaComponent } from './component/modal-atividade-academica/modal-atividade-academica.component'
import { ProfessorPortfolioComponent } from './pages/professor-portfolio/professor-portfolio.component'
import { PortfoliosComponent } from './component/portfolios/portfolios.component'
import { PortfolioComponent } from './component/portfolios/portfolio/portfolio.component';
import { ExploreComponent } from './pages/explore/explore.component';
import { CommentInputComponent } from './component/social-section/comments/comment-input/comment-input.component';
import { ExternalAccessComponent } from './pages/external-access/external-access.component';
import { PalmasComponent } from './component/social-section/palmas/palmas.component';
import { ModalConfirmationComponent } from './component/modal-confirmation/modal-confirmation.component'
import { HighlightComponent } from './pages/landing/highlight/highlight.component'
import { ModalShareComponent } from './component/modal-share/modal-share.component';


@NgModule({
  entryComponents: [
    ModalConfigUserComponent,
    ModalConfigPostComponent,
    ModalInitialPrivacyComponent,
    ModalSetPortfolioComponent,
    ModalAlertComponent,
    ModalEmbedComponent,
    ModalAtividadeAcademicaComponent,
    ModalShareComponent,
    ModalConfirmationComponent
    
  ],
  declarations: [
    AppComponent,
    LandingComponent,
    HeaderComponent,
    FooterComponent,
    UploadComponent,
    PopoverLoginComponent,
    RegisterProfileComponent,
    MyPortfolioComponent,
    HeaderPortfolioComponent,
    ModalConfigUserComponent,
    ModalInitialPrivacyComponent,
    NavMenuUserComponent,
    NewPublicationComponent,
    ModalConfigPostComponent,
    PublicationsComponent,
    PublicationComponent,
    LoaderComponent,
    ModalSetPortfolioComponent,
    TextPostComponent,
    ModalAlertComponent,
    ModalEmbedComponent,
    PublicationDetailComponent,
    UserInfoPublicationComponent,
    SocialSectionComponent,
    FeedbackSectionComponent,
    CommentsComponent,
    CommentComponent,
    FeedbackComponent,
    FeedbackInputComponent,
    ReflectionUserComponent,
    SanitizeHtmlPipe,
    ProfileComponent,
    TimelineComponent,
    ActivityComponent,
    ModalAtividadeAcademicaComponent,
    ProfessorPortfolioComponent,
    PortfoliosComponent,
    PortfolioComponent,
    ExploreComponent,
    CommentInputComponent,
    ExternalAccessComponent,
    PalmasComponent,
    ModalConfirmationComponent,
    HighlightComponent,
    ModalShareComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatTabsModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    MatButtonModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatCardModule,
    MatTooltipModule,
    StoreModule.forRoot(reducers),
    MediumEditorModule,
    ReactiveFormsModule,
    DragDropModule,
    EmbedVideo,
    MatSnackBarModule,
    SafePipeModule,
    MatListModule,
    MatSidenavModule,
    MatStepperModule,
    HttpClientModule,      // (Required) for share counts
    HttpClientJsonpModule, // (Optional) Add if you want Tumblr share counts
    ShareModule
  ],
  providers: [
    DatePipe,
    // {
    //   provide: HTTP_INTERCEPTORS,
    //   useClass: LoaderInterceptorService,
    //   multi: true
    // },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: GlobalErrorInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
