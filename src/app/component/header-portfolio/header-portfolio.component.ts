import { Component, OnInit, Input } from '@angular/core'
import { Router } from '@angular/router'
import { Store } from '@ngrx/store'
import { State } from '@ngrx/store'
import { AppState } from 'src/app/redux/app.state'
import { ACTIVE_MENU } from 'src/app/redux/constants'
import { ReplaySubject } from 'rxjs'
import { takeUntil } from 'rxjs/operators'


enum MENU {
  EXPLORAR = 0,
  MEUS_ALUNOS,
  MEU_PERFIL,
  NULL
}

@Component({
  selector: 'app-header-portfolio',
  templateUrl: './header-portfolio.component.html',
  styleUrls: ['./header-portfolio.component.scss']
})

export class HeaderPortfolioComponent implements OnInit {
  @Input() isNewPost = false

  activeButton = 1
  loggedUser = false
  professorHeader = false

  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1)

  constructor(
    private router: Router,
    private store: Store<AppState>,
    private state: State<AppState>) {
  }

  async ngOnInit() {
    this.checkPage()
    await this.state.pipe(takeUntil(this.destroyed$)).subscribe(async appState => {
      if (appState.auth.token !== null) {
        this.loggedUser = true
        this.professorHeader = (localStorage.getItem('professor_flag') === 'true' ? true : false)
        if (localStorage.getItem('activeHeader') !== null) {
          this.activeButton = Number(JSON.parse(localStorage.getItem('activeHeader')))
        }
      }
    })
  }


  checkPage() {
    switch (this.router.url) {
      case '/explorar': {
        this.setActive(MENU.EXPLORAR)
        break
      }
      case '/meu-portfolio': {
        this.setActive(MENU.MEU_PERFIL)
        break
      }
      case '/meus-alunos': {
        this.setActive(MENU.MEUS_ALUNOS)
        break
      }
    }
    // this.activeButton = Number(JSON.parse(localStorage.getItem('activeHeader')))
  }
  setActive(optionMenu: number) {
    this.store.dispatch({ type: ACTIVE_MENU, payload: optionMenu })
    localStorage.setItem('activeHeader', (optionMenu.toString()))
    this.activeButton = optionMenu
  }

  goPortfolio() {
    if (this.professorHeader) {
      this.router.navigate(['/meus-alunos'])
    } else {
      this.router.navigate(['/meu-portfolio'])
    }
  }
  ngOnDestroy() {
    this.destroyed$.next(true)
    this.destroyed$.complete()
  }
}
