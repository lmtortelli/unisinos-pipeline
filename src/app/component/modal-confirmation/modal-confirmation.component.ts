import { Component, OnInit } from '@angular/core'
import { MatDialogRef } from '@angular/material'

@Component({
  selector: 'app-modal-confirmation',
  templateUrl: './modal-confirmation.component.html',
  styleUrls: ['./modal-confirmation.component.scss']
})
export class ModalConfirmationComponent implements OnInit {


  areRemove = false

  constructor(
    public dialogRef: MatDialogRef<ModalConfirmationComponent>
  ) { }

  ngOnInit() {
  }

  remove() {
    this.areRemove = true
  }

  cancel() {
    this.areRemove = false
  }

}
