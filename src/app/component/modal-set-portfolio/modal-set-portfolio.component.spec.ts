import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalSetPortfolioComponent } from './modal-set-portfolio.component';

describe('ModalSetPortfolioComponent', () => {
  let component: ModalSetPortfolioComponent;
  let fixture: ComponentFixture<ModalSetPortfolioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalSetPortfolioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalSetPortfolioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
