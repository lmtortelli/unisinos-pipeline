import { Component, OnInit } from '@angular/core'
import { UsuarioModel } from 'src/app/models/usuario'
import { MatDialog } from '@angular/material'
import { Router } from '@angular/router'
import { RestapiService } from 'src/app/services/restapi.service'
import { AppState } from 'src/app/redux/app.state'
import { Store } from '@ngrx/store'
import { PROFESSOR_USER, ACTIVE_MENU } from 'src/app/redux/constants'

@Component({
  selector: 'app-modal-set-portfolio',
  templateUrl: './modal-set-portfolio.component.html',
  styleUrls: ['./modal-set-portfolio.component.scss']
})
export class ModalSetPortfolioComponent implements OnInit {

  isProfessor = false
  forms: any = {}
  user: UsuarioModel = <any>{}
  constructor(
    public dialog: MatDialog,
    private router: Router,
    private rest: RestapiService,
    private store: Store<AppState>) { }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('currentUser'))
  }

  setProfessor() {
    this.store.dispatch({ type: PROFESSOR_USER, payload: true})
    this.store.dispatch({ type: ACTIVE_MENU, payload: 0})
    localStorage.setItem('professor_flag', 'true')
    this.isProfessor = true
    this.setRoute()
  }

  setStudent() {
    this.store.dispatch({ type: PROFESSOR_USER, payload: false})
    localStorage.setItem('professor_flag', 'false')
    this.isProfessor = false
    this.setRoute()
  }

  setRoute() {
    this.dialog.closeAll()
    if (this.user.url) {
      const prof = localStorage.getItem('professor_flag')
      if (!this.isProfessor) {
        this.router.navigateByUrl('/meu-portfolio')
      } else {
        this.router.navigateByUrl('/meus-alunos')
      }
    } else {
      this.router.navigateByUrl('/cadastro')
    }
  }
}
