import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserInfoPublicationComponent } from './user-info-publication.component';

describe('UserInfoPublicationComponent', () => {
  let component: UserInfoPublicationComponent;
  let fixture: ComponentFixture<UserInfoPublicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserInfoPublicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserInfoPublicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
