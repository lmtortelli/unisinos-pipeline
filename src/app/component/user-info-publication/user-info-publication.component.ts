import { Component, OnInit, Input, SimpleChanges } from '@angular/core'
import { UsuarioModel } from 'src/app/models/usuario'
import { DatePipe } from '@angular/common'
import { DefaultService } from 'src/app/services/global/default.service'

@Component({
  selector: 'app-user-info-publication',
  templateUrl: './user-info-publication.component.html',
  styleUrls: ['./user-info-publication.component.scss']
})
export class UserInfoPublicationComponent implements OnInit {

  @Input()
  courseName: string

  @Input()
  className: string

  @Input()
  dtCadastro: string

  @Input()
  dtModificacao: string

  @Input()
  user: UsuarioModel

  @Input()
  isFirst: boolean

  dateCadastro: string
  dateModificacao: string

  constructor(
    public datepipe: DatePipe,
    private dService: DefaultService) { }

  ngOnInit() {
    this.dateCadastro = this.datepipe.transform(this.dtCadastro, 'dd/MM/yyyy HH:mm')
    this.dateModificacao = this.datepipe.transform(this.dtModificacao, 'dd/MM/yyyy HH:mm')
  }

  ngOnChanges(changes: SimpleChanges): void {
    for (const prop in changes) {
      if (prop === 'user') {
        if (this.user.img == null) {
          this.user.img = this.dService.standardUserImage()
        }
      }
    }
  }

}
