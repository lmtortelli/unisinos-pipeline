import { Component, OnInit, OnDestroy } from '@angular/core'
import { Subscription } from 'rxjs'
import { LoaderService } from '../../services/loader/loader.service'
import { LoaderState } from './loader.model'
import { Meta } from '@angular/platform-browser'
import { DefaultService } from 'src/app/services/global/default.service'

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent implements OnInit, OnDestroy {
  show = false
  private subscription: Subscription
  constructor(
    private loaderService: LoaderService,
    private dService: DefaultService,
    private meta: Meta) { }
  ngOnInit() {
    this.generateTags()
    this.subscription = this.loaderService.loaderState
      .subscribe((state: LoaderState) => {
        this.show = state.show
      })
  }
  ngOnDestroy() {
    this.subscription.unsubscribe()
  }

  generateTags() {
    // default values
    const config = {
      title : 'Portfólio Unisinos',
      description: 'VOCÊ FAZ PROJETOS INCRÍVEIS DURANTE A GRADUAÇÃO. CHEGOU A HORA DE MOSTRÁ-LOS AO MUNDO.',
      image : this.dService.logoUnisinos()
    }


    this.meta.updateTag({name : 'fb:app_id', content : '150985000664'})
    this.meta.updateTag({name : 'twitter:site:id', content : '28855413'})

    this.meta.updateTag({ name: 'twitter:card', content: 'summary' })
    this.meta.updateTag({ name: 'twitter:site', content: '@UnisinosPortfolio' })
    this.meta.updateTag({ name: 'twitter:title', content: config.title })
    this.meta.updateTag({ name: 'twitter:description', content: config.description })
    this.meta.updateTag({ name: 'twitter:image', content: config.image })

    this.meta.updateTag({ property: 'og:type', content: 'article' })
    this.meta.updateTag({ property: 'og:site_name', content: 'Unisinos Portfolio' })
    this.meta.updateTag({ property: 'og:title', content: config.title })
    this.meta.updateTag({ property: 'og:description', content: config.description })
    this.meta.updateTag({ property: 'og:image', content: config.image })
    this.meta.updateTag({ property: 'og:url', content: window.location.href})
  }


}
