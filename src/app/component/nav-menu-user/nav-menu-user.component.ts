import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core'
import { MatButtonModule } from '@angular/material/button'
import { UsuarioModel } from 'src/app/models/usuario'
import { Router } from '@angular/router'
import { RestapiService } from 'src/app/services/restapi.service'
import { MatDialogConfig, MatDialog } from '@angular/material'
import { ModalConfigUserComponent } from '../modal-config-user/modal-config-user.component'
import { State } from '@ngrx/store'
import { AppState } from 'src/app/redux/app.state'
import { ModalInitialPrivacyComponent } from '../modal-initial-privacy/modal-initial-privacy.component'
import { ModalConfirmationComponent } from '../modal-confirmation/modal-confirmation.component'
import { Observable, ReplaySubject } from 'rxjs'
import { takeUntil } from 'rxjs/operators'

@Component({
  selector: 'app-nav-menu-user',
  templateUrl: './nav-menu-user.component.html',
  styleUrls: ['./nav-menu-user.component.scss']
})

export class NavMenuUserComponent implements OnInit {
  user: UsuarioModel = <any>{}
  imageUser: string | ArrayBuffer
  professorFlag: boolean
  flagIcons = true
  @Input() isNewPublication = false
  @Output() eventUpdateButton = new EventEmitter()
  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1)

  constructor(
    private dialog: MatDialog,
    private router: Router,
    private state: State<AppState>,
    private rest: RestapiService) { }

  async ngOnInit() {
    await this.state.pipe(takeUntil(this.destroyed$)).subscribe(async appState => {
      this.user = await appState.auth.user.usuario
      this.professorFlag = (localStorage.getItem('professor_flag') === 'true' ? true : false)
      if (this.user.img == null) {
        this.imageUser = 'assets/img/perfil.png'
      } else {
        this.imageUser = this.user.img
      }
    })
  }

  goToPortfolio(): void {
    if (this.professorFlag) {
      this.router.navigate(['/meu-perfil'])
    } else {
      this.router.navigate(['/meu-portfolio'])
    }
  }

  NewPublication(): void {
    this.router.navigate(['/nova-publicacao'])
  }

  private logout(): void {
    const payload = { user: this.user }
    this.rest.postApi('usuario/logout', payload).pipe(takeUntil(this.destroyed$)).subscribe()
    localStorage.clear()
    this.router.navigate(['/home'])
  }

  openDialog(): void {
    const dialogConfig = new MatDialogConfig()
    dialogConfig.data = this.user
    dialogConfig.disableClose = true
    dialogConfig.autoFocus = true
    const dialogRef = this.dialog.open(ModalConfigUserComponent, dialogConfig)
  }

  ngOnDestroy() {
    this.destroyed$.next(true)
    this.destroyed$.complete()
  }
}
