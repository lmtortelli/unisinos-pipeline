import { Component, OnInit, Input, Inject } from '@angular/core'
import { UsuarioModel } from 'src/app/models/usuario'
import { RestapiService } from 'src/app/services/restapi.service'
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material'
import { ReplaySubject } from 'rxjs'
import { takeUntil } from 'rxjs/operators'

@Component({
  selector: 'app-modal-config-user',
  templateUrl: './modal-config-user.component.html',
  styleUrls: ['./modal-config-user.component.scss']
})
export class ModalConfigUserComponent implements OnInit {
  status = [true, false, false, false]
  user: UsuarioModel = <any>{}
  portfolioForm: any
  imagePath: string
  message: string
  forms: any = {}
  professorUser: boolean
  seasons: string[] = ['Apenas meus professores', 'Comunidade Acadêmica', 'Público']
  @Input() urlParcial: string
  biography: string
  template: any = {
    linkedin: 'https://www.linkedin.com/in/',
    twitter: 'https://twitter.com/',
    facebook: 'https://www.facebook.com/',
    lattes: 'http://lattes.cnpq.br/'
  }
  links: any = {
    linkedin: this.template.linkedin,
    twitter: this.template.twitter,
    facebook: this.template.facebook,
    lattes: this.template.lattes
  }
  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1)

  constructor(
    public dialogRef: MatDialogRef<ModalConfigUserComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private rest: RestapiService,
    private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('currentUser'))
    this.urlParcial = this.user.url.toLowerCase()
    this.buildCombo()
    this.forms.privacy = String(this.user.privacidade)
    const flgProf = localStorage.getItem('professor_flag')
    if (flgProf == 'true') {
      this.professorUser = true
    } else {
      this.professorUser = false
    }
    this.defaultUserUrl()


  }

  ngOnDestroy() {
    this.destroyed$.next(true)
    this.destroyed$.complete()
  }

  defaultUserUrl(): void {
    this.imagePath = '../../../assets/img/ok.png'
    this.message = 'Endereço disponível!'

  }

  onChange(value) {
    this.validateUrl(value)
  }

  clickEvent(option) {
    this.status = [false, false, false, false]
    this.status[option] = true
  }

  buildCombo() {
    this.links.linkedin += (this.user.lklinkedin) ? this.user.lklinkedin : ''
    this.links.twitter += (this.user.lktwitter) ? this.user.lktwitter : ''
    this.links.facebook += (this.user.lkfacebook) ? this.user.lkfacebook : ''
    this.links.lattes += (this.user.lklattes) ? this.user.lklattes : ''
    this.biography = this.user.biografia
  }

  save() {
    const payload = {
      lklinkedin: this.links.linkedin.replace(this.template.linkedin, ''),
      lktwitter: this.links.twitter.replace(this.template.twitter, ''),
      lkfacebook: this.links.facebook.replace(this.template.facebook, ''),
      lklattes: this.links.lattes.replace(this.template.lattes, ''),
      biografia: this.biography,
      url: this.urlParcial,
      privacidade: +this.forms.privacy
    }

    if (payload.lklinkedin == null) {
      delete payload.lklinkedin
    }
    if (payload.lktwitter == null) {
      delete payload.lktwitter
    }
    if (payload.lkfacebook == null) {
      delete payload.lkfacebook
    }
    if (payload.lklattes == null) {
      delete payload.lklattes
    }
    if (payload.biografia == null) {
      delete payload.biografia
    }
    if (payload.url == null) {
      delete payload.url
    }
    if (payload.privacidade == null) {
      delete payload.privacidade
    }

    if (this.validateUrl(this.urlParcial)) {
      this.rest.updateApi('usuario', payload, this.user.cdUsuario)
        .pipe(takeUntil(this.destroyed$)).subscribe(
          res => {
            localStorage.setItem('currentUser', JSON.stringify(res))
            this.errorSnackBar('Configurações atualizadas com sucesso!')
            location.reload()
          },
          error => {
            console.error('Erro no retorno do getUsuario: ' + error)
          }
        )
    }
  }

  errorSnackBar(message) {
    this.snackBar.open(message, null, {
      duration: 3500
    })
  }

  private validateUrl(url): boolean {
    if (url == this.user.url) {
      return true
    }
    this.rest.getByIdApi('usuario/url', url)
      .pipe(takeUntil(this.destroyed$)).subscribe(
        usuario => {
          if (usuario.length > 0) {
            this.imagePath = '../../../assets/img/erro.png'
            this.message = 'Endereço já utilizado'
            return false
          } else {
            this.user.url = url
            this.imagePath = '../../../assets/img/ok.png'
            this.message = 'Endereço disponível!'
            return true
          }
        },
        error => console.error('Erro no retorno do getUsuario: ' + error)
      )
    return false
  }

}
