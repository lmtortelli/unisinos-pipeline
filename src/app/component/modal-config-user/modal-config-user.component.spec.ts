import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalConfigUserComponent } from './modal-config-user.component';

describe('ModalConfigUserComponent', () => {
  let component: ModalConfigUserComponent;
  let fixture: ComponentFixture<ModalConfigUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalConfigUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalConfigUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
