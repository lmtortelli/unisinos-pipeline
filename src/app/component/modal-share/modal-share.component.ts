import { Component, OnInit, Input } from '@angular/core'
import { ShareService } from '@ngx-share/core'
import { MatSnackBar } from '@angular/material'

@Component({
  selector: 'app-modal-share',
  templateUrl: './modal-share.component.html',
  styleUrls: ['./modal-share.component.scss']
})
export class ModalShareComponent implements OnInit {

  @Input()
  url = window.location.href
  
  @Input()
  title: string

  @Input()
  desc: string

  constructor(
    private snackBar: MatSnackBar,
    public share: ShareService
  ) { }

  ngOnInit() { }

  copyMessage() {
    const selBox = document.createElement('textarea')
    selBox.style.position = 'fixed'
    selBox.style.left = '0'
    selBox.style.top = '0'
    selBox.style.opacity = '0'
    selBox.value = this.url
    document.body.appendChild(selBox)
    selBox.focus()
    selBox.select()
    document.execCommand('copy')
    document.body.removeChild(selBox)
    this.messageCopy()
  }

  messageCopy() {
    this.snackBar.open('URL copiada para área de transferência', null, {
      duration: 3500
    })
  }

}
