
import { Component, OnInit, Input, SimpleChanges, Output, EventEmitter } from '@angular/core'
import { Router } from '@angular/router'
import { AppState } from 'src/app/redux/app.state'
import { State, Store } from '@ngrx/store'
import { UsuarioModel } from 'src/app/models/usuario'
import { ActivityModel } from 'src/app/models/Activity.model'
import { RestapiService } from 'src/app/services/restapi.service'
import { DatePipe } from '@angular/common'
import { OrderSelect } from 'src/app/app.component'
import { ReplaySubject } from 'rxjs'
import { takeUntil } from 'rxjs/operators'


@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.scss']
})
export class TimelineComponent implements OnInit {


  @Input()
  private order = 0

  @Input()
  private offset = 0

  @Input()
  private timelineFilters: any = {}

  @Output()
  private activities = new EventEmitter<boolean>()

  @Output()
  private btnMoreActivies = new EventEmitter<boolean>()

  @Output()
  private show = new EventEmitter<boolean>()

  private hasActivitys = false

  private user: UsuarioModel = <any>{}
  logs: any = []
  private logMap = new Map()
  private imageUser: string
  private previousLength = 0

  private payload = {
    idPeriodoLetivo: '',
    idDisciplina: '',
    idAvaliacao: ''
  }
  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1)

  constructor(
    private store: Store<AppState>,
    private state: State<AppState>,
    private router: Router,
    private rest: RestapiService,
    public datepipe: DatePipe) {
    // this.logs = []
  }


  ngOnInit() {
    this.logMap.clear()
    this.logs = []
    this.state.pipe(takeUntil(this.destroyed$)).subscribe(async appState => {
      this.emitShow(true)
      // this.logs = []
      this.previousLength = 0
      this.logMap = new Map()

      this.user = await appState.auth.user.usuario
      if (this.user.url == null) {
        this.router.navigateByUrl('/cadastro')
      }
      if (this.buildPayloadFilters()) {
        this.getLogsTimeline()
      } else {
        this.getLogsByFilter()
      }


    })
  }

  ngOnChanges(changes: SimpleChanges) {
    for (const prop in changes) {
      if (prop === 'order') {
        this.onChangeOrder()
      } else if (prop === 'offset') {
        // this.logs = []
        this.MoreActivies()
      }
    }
  }

  MoreActivies() {
    if (this.buildPayloadFilters) { // If true means is empty
      this.getLogsTimeline()
    } else {
      this.getLogsByFilter()
    }
  }

  visibleButtonMorePublication() {
    if (this.previousLength === this.logMap.size) {
      this.btnMoreActivies.emit(false)
    } else {
      if ((this.logMap.size - this.previousLength) !== 10) {
        this.btnMoreActivies.emit(false)
      } else {
        this.btnMoreActivies.emit(true)
      }
      this.previousLength = this.logMap.size
    }
  }

  buildPayloadFilters() {
    this.payload = {
      idPeriodoLetivo: this.timelineFilters.periodSelected.idPeriodoLetivo,
      idDisciplina: this.timelineFilters.disciplineSelected.idDisciplina,
      idAvaliacao: this.timelineFilters.testSelected.idAvaliacao
    }

    if (this.timelineFilters.periodSelected === 'Todos') {
      delete this.payload.idPeriodoLetivo
    }
    if (this.timelineFilters.disciplineSelected === 'Todas') {
      delete this.payload.idDisciplina
    }
    if (this.timelineFilters.testSelected === 'Todas') {
      delete this.payload.idAvaliacao
    }

    if (Object.keys(this.payload).length === 0) {
      return true
    }

    return false


  }

  onChangeOrder() {
    this.logs = []
    this.logMap.forEach(val => {
      this.logs.push(val)
    })

    switch (this.order) {
      case OrderSelect.Recent: {
        this.logs.sort((a, b) => {
          return +new Date(a.dtLogEvento) - +new Date(b.dtLogEvento)
        })
        break
      }
      case OrderSelect.Old: {
        this.logs.reverse()
        break
      }
    }

  }

  getLogsTimeline() {
    this.rest.getByIdApi('timeline', + this.user.idUsuario + '/' + this.offset)
      .pipe(takeUntil(this.destroyed$)).subscribe(res => {
        if (res.length > 0) {
          res.forEach(element => {
            const act = <ActivityModel>element
            act['ownActivity'] = (act.idUsuario === this.user.idUsuario)
            act['dtLogEvento'] = this.datepipe.transform(act['dtLogEvento'], 'dd/MM/yyyy - HH:mm')
            if (!this.logMap.has(act.idLogEvento)) {
              this.activities.emit(true)
              this.logMap.set(act.idLogEvento, act)
            }
          })
          this.onChangeOrder()
          this.visibleButtonMorePublication()

        } else {
          this.activities.emit(false)
        }
        this.emitShow(false)
      }, error => {
        console.error('Erro ao buscar avaliações: ' + JSON.stringify(error))
      })
  }

  getLogsByFilter() {
    this.rest.postApi('timeline/' + this.user.idUsuario + '/' + this.offset, this.payload)
      .pipe(takeUntil(this.destroyed$)).subscribe(res => {
        if (res != null && res.length > 0) {
          this.activities.emit(true)
          res.forEach(element => {
            const act = <ActivityModel>element
            act['ownActivity'] = (act.idUsuario === this.user.idUsuario)
            act['dtLogEvento'] = this.datepipe.transform(act['dtLogEvento'], 'dd/MM/yyyy - HH:mm:ss')
            if (!this.logMap.has(act.idLogEvento)) {
              this.activities.emit(true)
              this.logMap.set(act.idLogEvento, act)
            }
          })
          this.onChangeOrder()
        } else {
          this.activities.emit(false)
        }
        this.emitShow(false)
      }, error => {
        console.error('Erro ao buscar avaliações: ' + JSON.stringify(error))
      })
  }

  emitShow(act: boolean) {
    try {
      this.show.emit(act)
    } catch (e) {
      console.log('Error emitShow', e)
    }
  }

  ngOnDestroy() {
    this.destroyed$.next(true)
    this.destroyed$.complete()
  }
}
