import { Component, OnInit, Input } from '@angular/core'
import { Activity } from 'src/app/app.component'
import { ActivityModel } from 'src/app/models/Activity.model'
import { UsuarioModel } from 'src/app/models/usuario'
import { AppState } from 'src/app/redux/app.state'
import { State } from '@ngrx/store'
import { Store } from '@ngrx/store'
import { Router } from '@angular/router'
import { ReplaySubject } from 'rxjs'
import { takeUntil } from 'rxjs/operators'

export enum Icon {
  REFRESH = 0,
  PAGE = 1,
  COMMENT = 2,
  USER = 3
}


@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.scss']
})
export class ActivityComponent implements OnInit {

  @Input()
  private activity: ActivityModel = new ActivityModel()

  private currentUser: UsuarioModel = <any>{}


  action: string
  icon: number
  isComment = false
  createdPortfolio = false
  private imageUser: string
  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1)

  constructor(
    private store: Store<AppState>,
    private state: State<AppState>,
    private router: Router
  ) { }

  ngOnInit() {

    this.state.pipe(takeUntil(this.destroyed$)).subscribe(async appState => {
      this.currentUser = await appState.auth.user.usuario
      if (this.currentUser.url == null) {
        this.router.navigateByUrl('/cadastro')
      }
      if (this.currentUser.img == null) {
        this.imageUser = 'assets/img/perfil.png'
      } else {
        this.imageUser = this.currentUser.img
      }
      this.buildInfoCard()
    })
  }

  goToPublication() {
    if (this.activity.publicacaoDesativado !== 1) {
      this.router.navigate([this.activity.publicacaoUrl])
    }
  }

  goToPortfolioAutor() {
    this.router.navigate([this.activity.autorUrl])
  }

  buildInfoCard() {

    if (this.activity.idTipoLogEvento === Activity.COMMENTS.ADD.ID) {
      if (this.currentUser.idUsuario === this.activity.idUsuario) {
        // Verificar se ID que vem e igual ao ID do usuario que esta vendo
        this.action = 'Você comentou em:'
        this.icon = Icon.COMMENT
        this.isComment = true
        if (this.activity.publicacaoImgAutor == null) {
          this.activity.publicacaoImgAutor = 'assets/img/perfil.png'
        }
      } else {
        this.action = this.activity.nomeUsuarioLog + ' comentou em:'
        this.icon = Icon.COMMENT
      }
    } else if (this.activity.idTipoLogEvento === Activity.FEEDBACK.ADD.ID) {
      if (this.currentUser.idUsuario === this.activity.idUsuario) {
        // Verificar se ID que vem e igual ao ID do usuario que esta vendo
        this.action = 'Você respondeu um feedback em:'
        this.icon = Icon.REFRESH
      } else {
        this.action = this.activity.nomeUsuarioLog + ' deixou um feedback em:'
        this.icon = Icon.REFRESH
      }
    } else if (this.activity.idTipoLogEvento === Activity.PUBLICATION.ADD.ID) {
      this.action = 'Você publicou:'
      this.icon = Icon.PAGE
    } else if (this.activity.idTipoLogEvento === Activity.PUBLICATION.EDIT.ID) {
      this.action = 'Você editou a publicação:'
      this.icon = Icon.PAGE
    } else if (this.activity.idTipoLogEvento === Activity.PUBLICATION.DELETE.ID) {
      this.action = 'Você removeu a publicação:'
      this.icon = Icon.PAGE
    } else if (this.activity.idTipoLogEvento === Activity.PORTFOLIO_CREATED.ID) {
      this.createdPortfolio = true
      this.action = 'Você criou seu portfólio'
      this.icon = Icon.USER
    }
  }

  ngOnDestroy() {
    this.destroyed$.next(true)
    this.destroyed$.complete()
  }

}
