import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalInitialPrivacyComponent } from './modal-initial-privacy.component';

describe('ModalInitialPrivacyComponent', () => {
  let component: ModalInitialPrivacyComponent;
  let fixture: ComponentFixture<ModalInitialPrivacyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalInitialPrivacyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalInitialPrivacyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
