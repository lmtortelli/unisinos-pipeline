import { Component, OnInit } from '@angular/core'
import { UsuarioService } from 'src/app/services/usuario/usuario.service'
import { MatDialog } from '@angular/material'
import { RestapiService } from 'src/app/services/restapi.service'
import { Router } from '@angular/router'
import { UsuarioModel } from 'src/app/models/usuario'
import { ReplaySubject } from 'rxjs'
import { takeUntil } from 'rxjs/operators'

@Component({
  selector: 'app-modal-initial-privacy',
  templateUrl: './modal-initial-privacy.component.html',
  styleUrls: ['./modal-initial-privacy.component.scss']
})
export class ModalInitialPrivacyComponent implements OnInit {
  forms: any = {}
  user: UsuarioModel = <any>{}
  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1)

  constructor(public dialog: MatDialog, private router: Router, private rest: RestapiService) { }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('currentUser'))
    this.forms.privacy = '0'
  }
  onSubmit() {
    const payload = { privacidade: +this.forms.privacy }
    this.rest.updateApi('usuario', payload, this.user.cdUsuario).pipe(takeUntil(this.destroyed$)).subscribe(
      res => {
        localStorage.setItem('currentUser', JSON.stringify(res))
        this.dialog.closeAll()
      },
      error => console.error('Erro no retorno do getUsuario: ' + error)
    )
  }
  ngOnDestroy() {
    this.destroyed$.next(true)
    this.destroyed$.complete()
  }
}
