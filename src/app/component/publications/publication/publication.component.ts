import { Component, OnInit, Input, Output, ComponentRef, ElementRef, OnDestroy } from '@angular/core'
import { PublicationModel } from 'src/app/models/publication-model'
import { UsuarioModel } from 'src/app/models/usuario'
import { MatDialogConfig, MatDialog } from '@angular/material'
import { ModalConfigPostComponent } from '../../modal-config-post/modal-config-post.component'
import { DatePipe } from '@angular/common'
import { RestapiService } from 'src/app/services/restapi.service'
import { Router, NavigationStart } from '@angular/router'
import { statusPublication } from 'src/app/app.component'
import { EventEmitter } from 'events'
import { AppState } from 'src/app/redux/app.state'
import { State } from '@ngrx/store'
import { DomSanitizer } from '@angular/platform-browser'
import { DefaultService } from 'src/app/services/global/default.service'
import { ReplaySubject } from 'rxjs'
import { takeUntil } from 'rxjs/operators'

@Component({
  selector: 'app-publication',
  templateUrl: './publication.component.html',
  styleUrls: ['./publication.component.scss']
})

export class PublicationComponent implements OnInit, OnDestroy {


  @Input() publication?: PublicationModel
  user: UsuarioModel = <any>{}
  currentUser: UsuarioModel = <any>{}
  imageUser: string
  hasFeedback = false
  hasPrivate = false
  date: string
  course: string
  disciplina: string
  private professorUser = false
  isActiveEdit = false
  countClaps = 0
  countVisualizaitons = 0

  isClaped = false
  statusPublication = `${statusPublication}`

  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1)

  constructor(
    private dialog: MatDialog,
    public datepipe: DatePipe,
    private dService: DefaultService,
    private rest: RestapiService,
    private router: Router,
    private state: State<AppState>,
    private sanitizer: DomSanitizer) { }


  ngOnInit() {
    this.state.pipe(takeUntil(this.destroyed$)).subscribe(async appState => {
          this.professorUser = await appState.view.isProfessor
          if (appState.auth.user != null) {
            this.currentUser = await appState.auth.user.usuario
            this.getPalmas(this.currentUser.idUsuario, this.publication.idPublicacao)
            if (this.publication.idUsuario === this.currentUser.idUsuario) {
              this.isActiveEdit = true
            }
          }
          this.getVisualization(this.publication.idPublicacao)
          this.getCountPalmas(this.publication.idPublicacao)

          if (this.publication.cdPermissao == 0) {
            this.hasPrivate = true
          }

          this.date = this.datepipe.transform(this.publication.dtCadastro, 'dd/MM/yyyy')
          if (this.publication.idTurma != null) {
            this.publicationTurma(this.publication.idTurma)
          }
          if (this.publication.idCurso != null) {
            this.publicationCourse(this.publication.idCurso)
          }
          if (this.publication.flagFeedback === 1) {
            this.hasFeedback = true
          }
          if (this.publication.imgPath != null) {
            this.publication.imgPath = this.sanitizer.bypassSecurityTrustUrl(this.publication.imgPath)
          }
          this.getUser(this.publication.idUsuario)
      })
  }

  getCountPalmas(idPublicacao: number) {
    this.rest.getApi('publicacao/count/palmas/' + idPublicacao).pipe(takeUntil(this.destroyed$)).subscribe(res => {
      if (res != null) {
        this.countClaps = res.count
      }
    })
  }

  getVisualization(idPublicacao: number) {
    this.rest.getApi('publicacao/visualizations/' + idPublicacao).pipe(takeUntil(this.destroyed$)).subscribe(res => {
      if (res != null) {
        this.countVisualizaitons = res.visualizacoes
      }
    })
  }

  getPalmas(idUsuario: number, idPublicacao: number) {
    this.rest.getApi('publicacao/palmas/' + idUsuario + '/' + idPublicacao).pipe(takeUntil(this.destroyed$)).subscribe(res => {
      if (res.length > 0) {
        this.isClaped = true
      } else {
        this.isClaped = false
      }
    })
  }

  private getUser(idUsuario: any) {
    this.rest.getByIdApi('usuario/alunoId', idUsuario).pipe(takeUntil(this.destroyed$)).subscribe(res => {
      if (res != null) {
        this.user = res[0] as UsuarioModel
        if (this.user.img == null) {
          this.imageUser = 'assets/img/perfil.png'
        } else {
          this.imageUser = this.user.img
        }
      }
    }, error => {
      console.error('Erro ao buscar publicações: ' + JSON.stringify(error))
    })
  }

  private publicationCourse(idCurso) {
    this.rest.getByIdApi('curso/get', idCurso).pipe(takeUntil(this.destroyed$)).subscribe(res => {
      this.course = res.nome
    }, error => {
      console.error('Erro ao buscar o curso: ' + error)
    })
  }

  private publicationTurma(idTurma) {
    this.rest.getByIdApi('disciplina/turma', idTurma).pipe(takeUntil(this.destroyed$)).subscribe(res => {
      this.disciplina = res[0].nome
    }, error => {
      console.error('Erro ao buscar o curso: ' + error)
    })
  }

  private openDialog(): void {
    const dialogConfig = new MatDialogConfig()
    dialogConfig.data = this.user
    dialogConfig.disableClose = true
    dialogConfig.autoFocus = true
    this.dialog.open(ModalConfigPostComponent, dialogConfig)
  }

  private onUpdate() {
    this.router.navigate(['/nova-publicacao'], { queryParams: { idPublicacao: this.publication.idPublicacao } })
  }

  private remove() {
    this.dService.confimationModal().pipe(takeUntil(this.destroyed$)).subscribe(success => {
      if (success) {
        this.rest.deleteApi('publicacao', this.publication.idPublicacao).pipe(takeUntil(this.destroyed$)).subscribe((data) => {
          location.reload()
        }, error => {
          console.error('Erro ao deletar feedback')
        })
      }
    })
  }
  ngOnDestroy() {
    this.destroyed$.next(true)
    this.destroyed$.complete()
  }
}
