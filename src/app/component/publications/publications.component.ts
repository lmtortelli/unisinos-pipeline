import { Component, OnInit, Input } from '@angular/core'
import { PublicationModel } from 'src/app/models/publication-model'

@Component({
  selector: 'app-publications',
  templateUrl: './publications.component.html',
  styleUrls: ['./publications.component.scss']
})
export class PublicationsComponent implements OnInit {
  @Input() publicacoes?: PublicationModel[]
  constructor() { }

  ngOnInit() {
  }

  updateComponent() {
    this.ngOnInit()
  }
}
