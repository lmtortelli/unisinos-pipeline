import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalConfigPostComponent } from './modal-config-post.component';

describe('ModalConfigPostComponent', () => {
  let component: ModalConfigPostComponent;
  let fixture: ComponentFixture<ModalConfigPostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalConfigPostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalConfigPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
