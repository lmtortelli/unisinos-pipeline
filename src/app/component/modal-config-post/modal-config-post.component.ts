import { Component, OnInit, Inject } from '@angular/core'
import { UsuarioModel } from 'src/app/models/usuario'
import { RestapiService } from 'src/app/services/restapi.service'
import { State } from '@ngrx/store'
import { AppState } from 'src/app/redux/app.state'
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material'
import { Router } from '@angular/router'

export interface Status {
  [status: string]: boolean
}

enum Active_Tab {
  basicData = 1,
  copyright = 2,
  privacyAccess = 3,
  feedbackSection = 4
}

@Component({
  selector: 'app-modal-config-post',
  templateUrl: './modal-config-post.component.html',
  styleUrls: ['./modal-config-post.component.scss']
})
export class ModalConfigPostComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ModalConfigPostComponent>, @Inject(MAT_DIALOG_DATA) public data: any,
    private rest: RestapiService, private state: State<AppState>, private router: Router, private snackBar: MatSnackBar) {
  }
  seasons: string[] = ['Apenas meus professores', 'Comunidade Acadêmica', 'Público']
  forms: any = {}
  user: UsuarioModel = <any>{}
  status = ['basicData', 'copyright', 'privacyAccess', 'reflection']
  selectedIndex = 0
  active_tab: any = Active_Tab.basicData
  selects: any = {
    period: {},
    disciplines: [{}],
    tests: [{}]
  }
  filters: any = {
    periodSelected: 'Todos semestres',
    disciplineSelected: 'Atividade Pessoal',
    testSelected: 'Nenhuma'
  }
  copyright = true
  privacy = '0'
  reflection = ''
  postConfig: any = {}
  configSaved = false
  idTurma: number = null
  post: any[]
  publication: any = null
  enableAvaliacao = false
  show = false

  private 

  next() {
    this.configSaved = false
    ++this.selectedIndex
  }
  previous() {
    this.configSaved = false
    --this.selectedIndex
  }
  ngOnInit() {
    if (this.filters.disciplineSelected === 'Atividade Pessoal') {
      this.enableAvaliacao = false
      this.filters.testSelected = 'Nenhuma'
    }
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.state.subscribe(async appState => {
        this.user = await appState.auth.user.usuario
        await this.getDisciplines()
        if (this.data.postConfig.idTurma === null) {
          await this.getTests()
        }
        await this.setPostConfig()
        // await this.setCombos()
      })
    })
  }

  setPostConfig() {
    this.post = this.data.post
    this.postConfig = this.data.postConfig
    this.copyright = this.data.postConfig.copyright
    this.privacy = this.data.postConfig.privacy.toString()
    this.reflection = this.data.postConfig.reflection
  }

  getDisciplines(payload = {}) {
    this.rest.postApi('disciplina/' + this.user.cdUsuario, payload).subscribe(res => {
      this.selects.disciplines = res
      if (this.data.postConfig.idTurma != null) {
        this.rest.getByIdApi('disciplina/turma', this.data.postConfig.idTurma).subscribe(res_disciplina => {
          res.forEach((element) => {
            if (res_disciplina[0].idDisciplina === element.idDisciplina) {
              this.filters.disciplineSelected = element
              this.enableAvaliacao = true
              if (this.data.postConfig.idAvaliacao != null) {
                const payload = { idDisciplina: element.idDisciplina }
                this.getTests(payload)
              }
              this.getTurmaByDisciplina(this.filters.disciplineSelected.idDisciplina)
            }
          })
        }, error => {
          console.error('Erro ao buscar disciplinas: ' + JSON.stringify(error))
        })
      }
    }, error => {
      console.error('Erro ao buscar disciplinas: ' + JSON.stringify(error))
    })
  }

  getPeriods() {
    this.rest.getByIdApi('periodo/usuario', this.user.cdUsuario).subscribe(res => {
      this.selects.periods = res
    }, error => {
      console.error('Erro ao buscar periodos: ' + JSON.stringify(error))
    })
  }

  getTests(payload = {}) {
    this.rest.postApi('avaliacao/' + this.user.cdUsuario, payload).subscribe(res => {
      this.selects.tests = res
      if (this.data.postConfig.idAvaliacao != null) {
        if (res != null) {
          res.forEach((element) => {
            if (this.data.postConfig.idAvaliacao === element.idAvaliacao) {
              this.filters.testSelected = element
            }
          })
        }
      }
    }, error => {
      console.error('Erro ao buscar avaliações: ' + JSON.stringify(error))
    })
  }

  getTurmaByDisciplina(idDisciplina) {
    this.rest.getByIdApi('turma/disciplina', idDisciplina + '/' + this.user.cdUsuario).subscribe(res => {
      this.idTurma = res.idTurma
    }, error => {
      console.error('Erro ao buscar avaliações: ' + JSON.stringify(error))
    })
  }

  getTurmaByAvaliacao(idAvaliacao) {
    this.rest.getByIdApi('turma/avaliacao', idAvaliacao).subscribe(res => {
      this.data.postConfig.idAvaliacao = idAvaliacao
      this.idTurma = res.idTurma
    }, error => {
      console.error('Erro ao buscar avaliações: ' + JSON.stringify(error))
    })
  }

  onChangeDisciplines(event) {
    if (this.filters.disciplineSelected === 'Atividade Pessoal') {
      this.enableAvaliacao = false
      this.filters.testSelected = 'Nenhuma'
    } else {
      this.enableAvaliacao = true
      const payload = { idDisciplina: event.idDisciplina }
      this.getTests(payload)
      this.getTurmaByDisciplina(event.idDisciplina)
    }
  }

  onChangeTests(event) {
    this.getTurmaByAvaliacao(event.idAvaliacao)
  }


  clickEvent(position) {
    this.configSaved = false
    this.selectedIndex = position
  }
  updateInfo() {
    this.postConfig = {
      disciplina: this.filters.disciplineSelected != null ? this.filters.disciplineSelected.idDisciplina : null,
      idAvaliacao: this.filters.testSelected != null ? this.filters.testSelected.idAvaliacao : null,
      copyright: this.copyright,
      privacy: +this.privacy,
      reflection: this.reflection,
      idTurma: this.idTurma,
      isDraft: this.data.postConfig.isDraft
    }
    if (this.filters.disciplineSelected === 'Atividade Pessoal') {
      this.postConfig.disciplina = null
      this.postConfig.idAvaliacao = null
      this.postConfig.idTurma = null
    }
    this.configSaved = true
  }

  private save() {
    this.updateInfo()
  }

  errorSnackBar(message) {
    this.snackBar.open(message, null, {
      duration: 3500
    })
  }

  verify() {
    if (!this.copyright) {
      this.configSaved = false
      this.errorSnackBar('Por favor, aceite a declarão de direitos autorais!')
      this.show = false
      this.selectedIndex = 1
      this.show = false
      return false
    }
    if (!this.postConfig.reflection) {
      this.configSaved = false
      this.errorSnackBar('Por favor, informe sua reflexão!')
      this.show = false
      return false
    }
    if (!this.data.payload[0].titulo) {
      this.dialogRef.close()
      this.errorSnackBar('Por favor, informe o título da publicação!')
      this.show = false
      return false
    }
    if (this.data.payload[0].conteudo == '[]') {
      this.dialogRef.close()
      this.errorSnackBar('Por favor, informe algum conteúdo na publicação!')
      this.show = false
      return false
    }
    return true
  }

  postPublication(isPubli) {
    this.show = true
    this.updateInfo()
    if (this.verify()) {
      // this.getTurmaByDisciplina(this.postConfig.idTurma)
      this.data.payload[0].idTurma = this.postConfig.idTurma
      this.data.payload[0].idAvaliacao = this.postConfig.idAvaliacao
      this.data.payload[0].cdPermissao = this.postConfig.privacy
      this.data.payload[0].reflexao = this.postConfig.reflection
      this.data.payload[0].cdStatus = this.postConfig.isDraft && !isPubli ? 0 : 1
      if (this.data.payload[0].dtCadastro == null) {
        this.data.payload[0].dtCadastro = new Date().toJSON().slice(0, 19).replace('T', ' ')
      }
      this.data.payload[0].dtPublicacao = new Date().toJSON().slice(0, 19).replace('T', ' ')
      if (this.data.draftId != null) {
        this.data.payload[0].dtModificacao = new Date().toJSON().slice(0, 19).replace('T', ' ')
        this.rest.updateApi('publicacao', this.data.payload, this.data.draftId).subscribe(
          res => {
            if (isPubli) {
              this.dialogRef.close()
              this.router.navigate(['/' + res.urlPublicacao])
            }
            this.show = false
          },
          error => {
            console.error('Erro no retorno do getUsuario: ' + error)
            this.show = false
          }
        )
      } else {
        this.postingPublication(this.data.payload, isPubli)
      }
    }
  }

  postingPublication(payload, isPubli) {
    this.rest.getByIdApi('publicacao/url', payload[0]['urlPublicacao']).subscribe(res => {
      if (res.count !== 0) {
        const qtdPubl = res.count
        const urlPublicacao = payload[0]['urlPublicacao'] + '-' + (qtdPubl + 1)
        payload[0]['urlPublicacao'] = urlPublicacao
      }
      this.rest.postApi('publicacao', payload).subscribe(res_post => {
        this.publication = res_post
        this.data.draftId = res_post[0].idPublicacao
        if (isPubli) {
          this.dialogRef.close()
          this.router.navigate(['/' + res_post[0].urlPublicacao])
        }
        this.show = false
      }, error => {
        console.error('Erro no retorno do getUsuario: ' + error)
        this.show = false
      })

    }, error => {
      console.error('Erro na criacao da URL: ' + error)
      this.show = false
    })
  }
}
