import { Component, OnInit, Input } from '@angular/core'
import { PortfolioModel } from 'src/app/models/portfolio.model'
import { DefaultService } from 'src/app/services/global/default.service'
import { getCurrencySymbol } from '@angular/common'
import { RestapiService } from 'src/app/services/restapi.service'
import { ReplaySubject } from 'rxjs'
import { takeUntil } from 'rxjs/operators'

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.scss']
})
export class PortfolioComponent implements OnInit {

  @Input()
  portfolio: PortfolioModel
  course: string
  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1)

  constructor(
    private rest: RestapiService,
    private dService: DefaultService) { }

  ngOnInit() {
    if (this.portfolio.img == null) {
      this.portfolio.img = this.dService.standardUserImage()
    }
    this.getCurso()
  }

  getCurso() {
    this.rest.getByIdApi('usuario/curso', this.portfolio.cdUsuario)
      .pipe(takeUntil(this.destroyed$)).subscribe(res => {
        res.cursos.forEach(element => {
          this.course = element.nome
        })
      }, error => {
        console.error('Erro ao buscar o curso: ' + error)
      })
  }
  ngOnDestroy() {
    this.destroyed$.next(true)
    this.destroyed$.complete()
  }
}
