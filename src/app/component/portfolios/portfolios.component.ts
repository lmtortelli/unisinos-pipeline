import { Component, OnInit, Input } from '@angular/core'
import { PortfolioModel } from 'src/app/models/portfolio.model'

@Component({
  selector: 'app-portfolios',
  templateUrl: './portfolios.component.html',
  styleUrls: ['./portfolios.component.scss']
})
export class PortfoliosComponent implements OnInit {

  @Input()
  portfolios?: PortfolioModel[]

  constructor() { }

  ngOnInit() {
  }

}
