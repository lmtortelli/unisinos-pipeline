import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core'
import { UsuarioModel } from 'src/app/models/usuario'
import { FeedbackModel } from 'src/app/models/feedback.model'
import { DatePipe } from '@angular/common'
import { DefaultService } from 'src/app/services/global/default.service'
import { RestapiService } from 'src/app/services/restapi.service'
import { AppState } from 'src/app/redux/app.state'
import { State } from '@ngrx/store'
import { ReplaySubject } from 'rxjs'
import { takeUntil } from 'rxjs/operators'

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit {

  @Input()
  private comment: FeedbackModel

  @Output()
  UpdateSocialComponent = new EventEmitter()

  currentUser: UsuarioModel

  user: UsuarioModel

  dtCadastro: string
  dtModificacao: string
  flagCanEdit = false
  flagActiveEdit = false

  dateCadastro: string
  dateModificado: string
  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1)

  constructor(
    public datepipe: DatePipe,
    private dService: DefaultService,
    private rest: RestapiService,
    private state: State<AppState>) { }

  ngOnInit() {
    this.state.pipe(takeUntil(this.destroyed$))
      .pipe(takeUntil(this.destroyed$)).subscribe(async appState => {
        this.currentUser = await appState.auth.user.usuario
        this.getUser(this.comment.idUsuario)
        this.dateCadastro = this.datepipe.transform(this.comment.dtCadastro, 'dd/MM/yyyy HH:mm')
        if (this.comment.dtModificado != null) {
          this.dateModificado = this.datepipe.transform(this.comment.dtModificado, 'dd/MM/yyyy - HH:mm')
        }
      })

  }


  private getUser(idUsuario: any) {
    this.rest.getByIdApi('usuario/alunoId', idUsuario).pipe(takeUntil(this.destroyed$)).subscribe(res => {
      if (res != null) {
        this.user = res[0] as UsuarioModel
        if (this.currentUser.idUsuario === this.user.idUsuario) {
          this.flagCanEdit = true
        }
        if (this.user.img == null) {
          this.user.img = this.dService.standardUserImage()
        }
      }
    }, error => {
      console.error('Erro ao buscar publicações: ' + JSON.stringify(error))
    })
  }

  remove() {
    this.dService.confimationModal().subscribe(success => {
      if (success) {
        this.rest.deleteApi('feedback', this.comment.idPublicacaoFeedback)
          .pipe(takeUntil(this.destroyed$)).subscribe((data) => {
            this.UpdateSocialComponent.emit()
          }, error => {
            console.error('Erro ao deletar feedback')
          })
      }
    })
  }

  onUpdate() {
    this.flagActiveEdit = true
  }

  ngOnDestroy() {
    this.destroyed$.next(true)
    this.destroyed$.complete()
  }
}
