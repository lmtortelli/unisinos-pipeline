import { Component, OnInit, Output, Input } from '@angular/core'
import { OuterSubscriber } from 'rxjs/internal/OuterSubscriber'
import { RestapiService } from 'src/app/services/restapi.service'
import { FeedbackModel } from 'src/app/models/feedback.model'
import { takeUntil } from 'rxjs/operators'
import { ReplaySubject } from 'rxjs'

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements OnInit {

  @Output()
  comments: any = []

  @Input()
  idPublicacao: number

  isLogged = false
  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1)

  constructor(private rest: RestapiService) { }

  ngOnInit() {
    this.getComments()
    // this.comments = ['C1', 'C2', 'C3', 'C4']
  }

  getComments() {
    this.comments = []
    this.rest.getByIdApi('comment/publicacao', this.idPublicacao)
      .pipe(takeUntil(this.destroyed$)).subscribe(res => {
        if (res.length > 0) {
          res.forEach(element => {
            element['isDashed'] = true
            this.comments.push(<FeedbackModel>element)
          })
          this.changeOrder()
        }
      }, error => {
        console.error('Erro ao buscar comentários: ' + JSON.stringify(error))
      })
  }

  UpdateComponent() {
    this.ngOnInit()
  }

  changeOrder() {
    this.comments.sort((a, b) => (a.dtCadastro < b.dtCadastro) ? 1 : -1)
    this.comments[this.comments.length - 1].isDashed = false
  }

  ngOnDestroy() {
    this.destroyed$.next(true)
    this.destroyed$.complete()
  }
}
