import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PalmasComponent } from './palmas.component';

describe('PalmasComponent', () => {
  let component: PalmasComponent;
  let fixture: ComponentFixture<PalmasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PalmasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PalmasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
