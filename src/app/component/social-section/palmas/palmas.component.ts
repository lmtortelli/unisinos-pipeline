import { Component, OnInit, Input } from '@angular/core'
import { State } from '@ngrx/store'
import { AppState } from 'src/app/redux/app.state'
import { UsuarioModel } from 'src/app/models/usuario'
import { RestapiService } from 'src/app/services/restapi.service'
import { Router } from '@angular/router'
import { DefaultService } from 'src/app/services/global/default.service'
import { MatDialog, MatDialogConfig } from '@angular/material'
import { ModalShareComponent } from '../../modal-share/modal-share.component'
import { ReplaySubject } from 'rxjs'
import { takeUntil } from 'rxjs/operators'

@Component({
  selector: 'app-palmas',
  templateUrl: './palmas.component.html',
  styleUrls: ['./palmas.component.scss']
})
export class PalmasComponent implements OnInit {

  private user: UsuarioModel = <any>{}
  private show: boolean
  palmasActive = false
  visualizations: number
  canEdit = false
  countClaps: number
  titulo: string
  @Input()
  private idPublicacao: number
  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1)

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private rest: RestapiService,
    private dService: DefaultService,
    private state: State<AppState>) { }

  async ngOnInit() {
    await this.state.pipe(takeUntil(this.destroyed$)).subscribe(async appState => {
      if (appState.auth.token !== null) {
        this.visualizations = 0
        this.user = await appState.auth.user.usuario
        this.getPalmas(this.user.idUsuario, this.idPublicacao)
        this.checkAutorForEdit(this.user.idUsuario, this.idPublicacao)
        await this.getVisualization(this.idPublicacao)
        await this.setVisualization(this.idPublicacao)
        await this.getTitulo(this.idPublicacao)
        this.show = true
      } else {
        this.show = false
      }

    })
  }

  getPalmas(idUsuario: number, idPublicacao: number) {
    this.getCountPalmas(idPublicacao)
    this.rest.getApi('publicacao/palmas/' + idUsuario + '/' + idPublicacao)
      .pipe(takeUntil(this.destroyed$)).subscribe(res => {
        if (res.length > 0) {
          this.palmasActive = true
        } else {
          this.palmasActive = false
        }
      })
  }

  getCountPalmas(idPublicacao: number) {
    this.rest.getApi('publicacao/count/palmas/' + idPublicacao)
      .pipe(takeUntil(this.destroyed$)).subscribe(res => {
        if (res != null) {
          this.countClaps = res.count
        }
      })
  }

  checkAutorForEdit(idUsuario: number, idPublicacao: number) {
    this.rest.getApi('publicacao/checkAutor/' + idPublicacao + '/' + idUsuario)
      .pipe(takeUntil(this.destroyed$)).subscribe(res => {
        this.canEdit = res.isAutor
      })
  }


  private editPublication() {
    this.router.navigate(['/nova-publicacao'], { queryParams: { idPublicacao: this.idPublicacao } })
  }

  private remove() {
    this.dService.confimationModal()
      .pipe(takeUntil(this.destroyed$)).subscribe(success => {
        if (success) {
          this.rest.deleteApi('publicacao', this.idPublicacao)
            .pipe(takeUntil(this.destroyed$)).subscribe((data) => {
              this.router.navigateByUrl('/meu-portfolio')
            }, error => {
              console.error('Erro ao deletar feedback')
            })
        }
      })
  }

  submitPalmas() {
    const payload = {
      idUsuario: this.user.idUsuario,
      idPublicacao: this.idPublicacao
    }
    this.rest.postApi('publicacao/palmas', payload)
      .pipe(takeUntil(this.destroyed$)).subscribe(res => {
        this.getCountPalmas(this.idPublicacao)
        this.palmasActive = !this.palmasActive
      }, error => {
        console.error('Erro ao enviar palmas: ' + error)
      })
  }

  getVisualization(idPublicacao: number) {
    this.rest.getApi('publicacao/visualizations/' + idPublicacao)
      .pipe(takeUntil(this.destroyed$)).subscribe(res => {
        if (res != null) {
          this.visualizations = res.visualizacoes
        }
      })
  }

  getTitulo(idPublicacao: number) {
    this.rest.getApi('publicacao/title/' + idPublicacao)
      .pipe(takeUntil(this.destroyed$)).subscribe(res => {
        if (res != null) {
          this.titulo = res.title
        }
      })
  }

  setVisualization(idPublicacao: number) {
    const visualization = localStorage.getItem(idPublicacao.toString())
    if (visualization === null) {
      this.rest.postApi('publicacao/visualizacao/' + idPublicacao, {})
        .pipe(takeUntil(this.destroyed$)).subscribe(res => {
          localStorage.setItem(idPublicacao.toString(), 'true')
        }, error => {
          console.error('Erro ao publicar feedback: ' + error)
        })
    }
  }

  shareClick() {
    const dialogConfig = new MatDialogConfig()
    dialogConfig.data = this.user
    dialogConfig.disableClose = true
    dialogConfig.autoFocus = true
    const dialogRef = this.dialog.open(ModalShareComponent, dialogConfig)
    dialogRef.componentInstance.desc = this.user.nome
    dialogRef.componentInstance.title = this.titulo
  }



  ngOnDestroy() {
    this.destroyed$.next(true)
    this.destroyed$.complete()
  }
}
