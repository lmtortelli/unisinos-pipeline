import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core'
import { UsuarioModel, Usuario } from 'src/app/models/usuario'
import { DatePipe } from '@angular/common'
import { FeedbackModel } from 'src/app/models/feedback.model'
import { RestapiService } from 'src/app/services/restapi.service'
import { AppState } from 'src/app/redux/app.state'
import { State } from '@ngrx/store'
import { DefaultService } from 'src/app/services/global/default.service'
import { takeUntil } from 'rxjs/operators'
import { ReplaySubject } from 'rxjs'

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss']
})
export class FeedbackComponent implements OnInit {
  @Output()
  UpdateSocialComponent = new EventEmitter()

  @Input()
  feedbackInfo: FeedbackModel
  currentUser: UsuarioModel
  user: UsuarioModel
  dtCadastro: string
  dtModificacao: string
  flagCanEdit = false
  flagActiveEdit = false
  dateCadastro: string
  dateModificado: string

  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1)

  constructor(
    public datepipe: DatePipe,
    private dService: DefaultService,
    private rest: RestapiService,
    private state: State<AppState>) { }

  ngOnInit() {
    this.state.pipe(takeUntil(this.destroyed$)).subscribe(async appState => {
      this.currentUser = await appState.auth.user.usuario

    })
    this.getUser(this.feedbackInfo.idUsuario)
    this.dateCadastro = this.datepipe.transform(this.feedbackInfo.dtCadastro, 'dd/MM/yyyy HH:mm')
    if (this.feedbackInfo.dtModificado != null) {
      this.dateModificado = this.datepipe.transform(this.feedbackInfo.dtModificado, 'dd/MM/yyyy HH:mm')
    }
  }

  private getUser(idUsuario: any) {
    this.rest.getByIdApi('usuario/alunoId', idUsuario)
      .pipe(takeUntil(this.destroyed$)).subscribe(res => {
        if (res != null) {
          this.user = res[0] as UsuarioModel
          if (this.currentUser.idUsuario === this.user.idUsuario) {
            this.flagCanEdit = true
          }
          if (this.user.img == null) {
            this.user.img = this.dService.standardUserImage()
          }
        }
      }, error => {
        console.error('Erro ao buscar publicações: ' + JSON.stringify(error))
      })
  }

  remove() {
    this.dService.confimationModal().pipe(takeUntil(this.destroyed$)).subscribe(success => {
      if (success) {
        this.rest.deleteApi('feedback', this.feedbackInfo.idPublicacaoFeedback)
          .pipe(takeUntil(this.destroyed$)).subscribe((data) => {
            this.UpdateSocialComponent.emit()
          }, error => {
            console.error('Erro ao deletar feedback')
          })
      }
    })
  }

  onUpdate() {
    this.flagActiveEdit = true
  }

  ngOnDestroy() {
    this.destroyed$.next(true)
    this.destroyed$.complete()
  }
}
