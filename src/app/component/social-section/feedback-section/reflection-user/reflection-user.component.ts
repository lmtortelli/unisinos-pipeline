import { Component, OnInit, Input } from '@angular/core'
import { DefaultService } from 'src/app/services/global/default.service'

@Component({
  selector: 'app-reflection-user',
  templateUrl: './reflection-user.component.html',
  styleUrls: ['./reflection-user.component.scss']
})
export class ReflectionUserComponent implements OnInit {

  @Input()
  text: string

  @Input()
  imgPath: string

  constructor(
    public dService: DefaultService
  ) { }

  ngOnInit() {
  if (this.imgPath == null) {
    this.imgPath = this.dService.standardUserImage()
    }
  }

}
