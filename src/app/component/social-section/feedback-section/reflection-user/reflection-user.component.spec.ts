import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReflectionUserComponent } from './reflection-user.component';

describe('ReflectionUserComponent', () => {
  let component: ReflectionUserComponent;
  let fixture: ComponentFixture<ReflectionUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReflectionUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReflectionUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
