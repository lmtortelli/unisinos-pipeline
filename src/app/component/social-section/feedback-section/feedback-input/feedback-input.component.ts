import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core'
import { UsuarioModel } from 'src/app/models/usuario'
import { State } from '@ngrx/store'
import { AppState } from 'src/app/redux/app.state'
import { RestapiService } from 'src/app/services/restapi.service'
import { Router } from '@angular/router'
import { DefaultService } from 'src/app/services/global/default.service'
import { MatSnackBar } from '@angular/material'
import { takeUntil } from 'rxjs/operators';
import { ReplaySubject } from 'rxjs';

@Component({
  selector: 'app-feedback-input',
  templateUrl: './feedback-input.component.html',
  styleUrls: ['./feedback-input.component.scss']
})
export class FeedbackInputComponent implements OnInit {
  user: UsuarioModel

  @Output()
  UpdateSocialComponent = new EventEmitter()

  @Input()
  idPublicacao?: number

  @Input()
  idPublicacaoFeedback?: number

  @Input()
  flagEdit: boolean

  @Input()
  textUpdate: string

  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1)

  constructor(
    private state: State<AppState>,
    private rest: RestapiService,
    private dService: DefaultService,
    private router: Router,
    private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.state.pipe(takeUntil(this.destroyed$)).subscribe(async appState => {
      this.user = await appState.auth.user.usuario
      if (this.user.img == null) {
        this.user.img = this.dService.standardUserImage()
      }
    })

    if (this.flagEdit == null) {
      this.flagEdit = false
    }
  }

  errorSnackBar(message) {
    this.snackBar.open(message, null, {
      duration: 3500
    })
  }

  submitFeedback(text: string) {
    // Not edit, input
    if (!this.flagEdit) {
      const payload = [{
        idUsuario: this.user.idUsuario,
        idPublicacao: this.idPublicacao,
        texto: text,
        flagProfessor: this.user.flagProfessor
      }]
      this.rest.postApi('feedback', payload)
        .pipe(takeUntil(this.destroyed$)).subscribe(res => {
          if (localStorage.getItem('professor_flag') === 'true') {
            const updatePublicacao = [{
              flagFeedback: 1
            }]
            this.rest.updateApi('publicacao', updatePublicacao, this.idPublicacao)
              .pipe(takeUntil(this.destroyed$))
              .subscribe(res => { },
                error => {
                  console.error('Erro ao atualizar feedback: ' + error)
                })
          }
          this.textUpdate = ' '
          this.errorSnackBar('Feedback enviado!')
          this.UpdateSocialComponent.emit()

        }, error => {
          console.error('Erro ao publicar feedback: ' + error)
        })
    } else {
      const payload = { texto: text, dtModificado: new Date().toISOString().slice(0, 19).replace('T', ' ') }
      this.rest.updateApi('feedback', payload, this.idPublicacaoFeedback)
        .pipe(takeUntil(this.destroyed$)).subscribe(res => {
          this.textUpdate = ' '
          this.UpdateSocialComponent.emit()
          this.errorSnackBar('Feedback atualizado!')
        }, error => {
          console.error('Erro ao atualizar feedback: ' + error)
        })
    }
    this.textUpdate = 'Deixe seu comentário.'
  }

  ngOnDestroy() {
    this.destroyed$.next(true)
    this.destroyed$.complete()
  }
}
