import { Component, OnInit, Input, Output } from '@angular/core'
import { MatListModule } from '@angular/material/list'
import { FeedbackModel } from 'src/app/models/feedback.model'
import { RestapiService } from 'src/app/services/restapi.service'
import { DefaultService } from 'src/app/services/global/default.service'
import { ReplaySubject } from 'rxjs'
import { takeUntil } from 'rxjs/operators'

@Component({
  selector: 'app-feedback-section',
  templateUrl: './feedback-section.component.html',
  styleUrls: ['./feedback-section.component.scss']
})
export class FeedbackSectionComponent implements OnInit {

  @Input()
  idPublicacao: number

  @Input()
  imgPath: string

  @Input()
  reflexao: string

  @Input()
  visibleFeedback = false

  firstFeedBack = true

  feedbacks: any = []

  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1)

  constructor(
    private rest: RestapiService,
    private dService: DefaultService
  ) { }

  ngOnInit() {
    this.getFeedbacks()
    if (this.imgPath == null) {
      this.imgPath = this.dService.standardUserImage()
    }
  }

  getFeedbacks() {

    this.feedbacks = []
    this.rest.getByIdApi('feedback/publicacao', this.idPublicacao)
      .pipe(takeUntil(this.destroyed$)).subscribe(res => {
        if (res.length > 0) {
          res.forEach(element => {
            this.feedbacks.push(<FeedbackModel>element)
          })
          this.firstFeedBack = false
        }
      }, error => {
        console.error('Erro ao buscar Feedbacks: ' + JSON.stringify(error))
      })
  }

  UpdateComponent() {
    this.ngOnInit()
  }

  ngOnDestroy() {
    this.destroyed$.next(true)
    this.destroyed$.complete()
  }
}
