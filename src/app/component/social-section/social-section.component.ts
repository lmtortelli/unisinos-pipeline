import { Component, OnInit, Input, ViewChild } from '@angular/core'
import { startWith, tap, takeUntil } from 'rxjs/operators'
import { MatTabsModule } from '@angular/material'
import { FeedbackModel } from 'src/app/models/feedback.model'
import { RestapiService } from 'src/app/services/restapi.service'
import { State } from '@ngrx/store'
import { AppState } from 'src/app/redux/app.state'
import { ReplaySubject } from 'rxjs'

@Component({
  selector: 'app-social-section',
  templateUrl: './social-section.component.html',
  styleUrls: ['./social-section.component.scss']
})
export class SocialSectionComponent implements OnInit {
  @Input()
  idPublicacao: number

  @Input()
  reflexao: string

  @Input()
  imgPath: string

  @Input()
  visibleFeedback: boolean

  tab = 1
  socialSection = true

  idCurrentUser: number
  feedbacks: any = []
  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1)

  constructor(private state: State<AppState>) {
    this.visibleFeedback = false
  }

  async ngOnInit() {
    await this.state.pipe(takeUntil(this.destroyed$)).subscribe(async appState => {
      if (appState.auth.user === null) {
        this.socialSection = false
      }
    })
  }

  changeTab(value: number) {
    this.tab = value
  }

  ngOnDestroy() {
    this.destroyed$.next(true)
    this.destroyed$.complete()
  }
}
