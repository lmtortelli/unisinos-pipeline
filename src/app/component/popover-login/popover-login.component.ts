import { Component, OnInit, ViewChild, ElementRef, Input, SimpleChanges } from '@angular/core'
import { Router } from '@angular/router'
import { Store } from '@ngrx/store'

import { RestapiService } from 'src/app/services/restapi.service'
import { AppState } from 'src/app/redux/app.state'
import { FETCH_USER_SUCCESS, SET_TOKEN, PROFESSOR_USER } from 'src/app/redux/constants'
import { MatDialog, MatDialogConfig } from '@angular/material'
import { ModalSetPortfolioComponent } from '../modal-set-portfolio/modal-set-portfolio.component'
import { NgbPopover } from '@ng-bootstrap/ng-bootstrap'
import { takeUntil } from 'rxjs/operators'
import { ReplaySubject } from 'rxjs'

@Component({
  selector: 'app-popover-login',
  templateUrl: './popover-login.component.html',
  styleUrls: ['./popover-login.component.scss']
})

export class PopoverLoginComponent implements OnInit {
  model: any = {}
  errorBool = false
  errorMessage: any
  user: any
  show = false

  @Input()
  isOpen = false

  @ViewChild('loginPopover')
  loginPopover: NgbPopover

  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1)

  constructor(private router: Router,
    private rest: RestapiService,
    private store: Store<AppState>,
    public dialog: MatDialog) {

  }

  ngOnInit() {
    if (this.isOpen) {
      setTimeout(() => this.loginPopover.open())
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    for (const prop in changes) {
      if (prop === 'isOpen') {
        if (JSON.stringify(changes[prop].currentValue) === 'true') {
          setTimeout(() => this.loginPopover.open())
        } else {
          setTimeout(() => this.loginPopover.close())
        }
      }
    }
  }

  onSubmit() {
    localStorage.clear()
    this.show = true
    localStorage.setItem('activeHeader', '1')
    const payload = { user: this.model.cdUsuario, pass: this.model.password }
    // this.rest.postApi('usuario/login', payload).pipe(takeUntil(this.destroyed$)).subscribe(
    this.rest.postApi('usuario/loginLocal', payload).pipe(takeUntil(this.destroyed$)).subscribe(
      res => {
        if (res.err) {
          this.errorBool = true
        } else {
          this.store.dispatch({ type: PROFESSOR_USER, payload: false })
          localStorage.setItem('token', res.token)
          this.store.dispatch({ type: SET_TOKEN, payload: res.token })
          this.store.dispatch({ type: FETCH_USER_SUCCESS, payload: res })
          this.loginPopover.close()
          // TODO: remove
          setTimeout(() => this.modalSetAccount(res))
        }
        this.show = false
      },
      error => {
        console.error('Erro ao publicar feedback: ' + error)
        this.show = false
      }
    )
  }

  modalSetAccount(res: any) {
    localStorage.setItem('currentUser', JSON.stringify(res))
    this.user = JSON.parse(localStorage.getItem('currentUser'))
    if (this.user.flagProfessor === 1) {
      localStorage.setItem('professor_flag', 'true')
    } else {
      localStorage.setItem('professor_flag', 'false')
    }

    if (this.user.flagProfessor === 1 && this.user.flagAluno === 1) {
      const dialogConfig = new MatDialogConfig()
      dialogConfig.disableClose = true
      dialogConfig.autoFocus = true
      this.dialog.open(ModalSetPortfolioComponent, dialogConfig)
    } else if (this.user.url) {
      if (this.user.flagProfessor == 0) {
        this.router.navigateByUrl('/meu-portfolio')
      } else {
        this.router.navigateByUrl('/meus-alunos')
      }

    } else {
      this.router.navigateByUrl('/cadastro')
    }
  }
  ngOnDestroy() {
    this.destroyed$.next(true)
    this.destroyed$.complete()
  }
}
