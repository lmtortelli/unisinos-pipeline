import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalEmbedComponent } from './modal-embed.component';

describe('ModalEmbedComponent', () => {
  let component: ModalEmbedComponent;
  let fixture: ComponentFixture<ModalEmbedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalEmbedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalEmbedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
