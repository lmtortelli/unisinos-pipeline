import { Component, OnInit, Inject } from '@angular/core'
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material'

@Component({
  selector: 'app-modal-embed',
  templateUrl: './modal-embed.component.html',
  styleUrls: ['./modal-embed.component.scss']
})
export class ModalEmbedComponent implements OnInit {
  urlEmbed: string
  isVideo: boolean
  constructor(public dialogRef: MatDialogRef<ModalEmbedComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
    this.urlEmbed = ''
    this.isVideo = this.data === 'video' ? true : false
  }

  ngOnInit() {
  }

}
