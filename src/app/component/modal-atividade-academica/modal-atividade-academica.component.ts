import { Component, OnInit, Inject, Input } from '@angular/core'
import { UsuarioModel } from 'src/app/models/usuario'
import { ProfessorAtividadeAcademica } from 'src/app/models/professorAtividadeAcademica'
import { RestapiService } from 'src/app/services/restapi.service'
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material'
import { AppState } from 'src/app/redux/app.state'
import { Store, State } from '@ngrx/store'
import { DefaultService } from 'src/app/services/global/default.service'
import { takeUntil } from 'rxjs/operators'
import { ReplaySubject } from 'rxjs'

@Component({
  selector: 'app-modal-atividade-academica',
  templateUrl: './modal-atividade-academica.component.html',
  styleUrls: ['./modal-atividade-academica.component.scss']
})
export class ModalAtividadeAcademicaComponent implements OnInit {

  user: UsuarioModel = <any>{}

  private professor


  disciplines: any = new Map()
  private data: any = {
    idDisciplina: '',
    nomeDisciplina: '',
    nomeCurso: '',
    professor: []
  }
  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1)

  constructor(
    public dialogRef: MatDialogRef<ModalAtividadeAcademicaComponent>,
    private state: State<AppState>,
    private rest: RestapiService,
    private dService: DefaultService,
    private store: Store<AppState>) { }

  async ngOnInit() {
    this.getAtividadesAcademicas()
  }

  ngOnDestroy() {
    this.destroyed$.next(true)
    this.destroyed$.complete()
    this.dialogRef.close()
  }

  getAtividadesAcademicas() {
    this.rest.getByIdApi('usuario/disciplinas', this.user.idUsuario)
      .pipe(takeUntil(this.destroyed$)).subscribe(res => {
        if (res != null && res.length >= 1) {
          res.forEach((element) => {
            if (!this.disciplines.has(element.idDisciplina)) {
              const data = {
                idDisciplina: element.idDisciplina,
                nomeDisciplina: element.nomeDisciplina,
                nomeCurso: element.nomeCurso,
                professor: []
              }
              this.disciplines.set(element.idDisciplina, data)
            }
            const prof: ProfessorAtividadeAcademica = {
              nome: element.nomeProfessor,
              img: (element.imgProfessor !== null) ? element.imgProfessor : this.user.img = this.dService.standardUserImage(),
              url: element.urlProfessor
            }
            this.disciplines.get(element.idDisciplina).professor.push(prof)
          })
        }
      }, error => {
        console.error('Erro ao buscar disciplinas: ' + JSON.stringify(error))
      })
  }
}


