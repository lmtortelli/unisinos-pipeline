import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAtividadeAcademicaComponent } from './modal-atividade-academica.component';

describe('ModalAtividadeAcademicaComponent', () => {
  let component: ModalAtividadeAcademicaComponent;
  let fixture: ComponentFixture<ModalAtividadeAcademicaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalAtividadeAcademicaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAtividadeAcademicaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
