import { Injectable } from '@angular/core'
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router'
import { Store, State } from '@ngrx/store'

import { AppState } from '../redux/app.state'
import { fetchUserOpenGuard } from '../redux/actions'
import { UsuarioService } from '../services/usuario/usuario.service'
import { SET_TOKEN } from '../redux/constants'

@Injectable({
  providedIn: 'root'
})

export class OpenGuard implements CanActivate {
  auth: any

  constructor(
    private router: Router,
    private userService: UsuarioService,
    private store: Store<AppState>,
    private state: State<AppState>
  ) {
    this.state.subscribe(appState => this.auth = appState.auth)
  }

  async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    // TODO: Verificar se precisa da seguinte condição pois esta atrapalhando para pegar as informações do usuário ao logar
    // if (this.auth.user) {
    //   return true
    // }
    await this.store.dispatch({ type: SET_TOKEN, payload: localStorage.getItem('token') })

    await fetchUserOpenGuard(this.store, this.userService)

    return true
    }


}
