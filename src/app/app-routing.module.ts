import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { LandingComponent } from './pages/landing/landing.component'
import { UploadComponent } from './pages/upload/upload.component'
import { RegisterProfileComponent } from './pages/register-profile/register-profile.component'
import { MyPortfolioComponent } from './pages/my-portfolio/my-portfolio.component'
import { AuthGuard } from './auth/auth-guard'
import { NewPublicationComponent } from './pages/new-publication/new-publication.component'
import { PublicationDetailComponent } from './pages/publication-detail/publication-detail.component'
import { ProfileComponent } from './pages/profile/profile.component'
import { ProfessorPortfolioComponent } from './pages/professor-portfolio/professor-portfolio.component'
import { ExploreComponent } from './pages/explore/explore.component'
import { ExternalAccessComponent } from './pages/external-access/external-access.component'
import { OpenGuard } from './auth/open-guard'

const routes: Routes = [
    { path: 'home', component: LandingComponent },
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'upload', component: UploadComponent },
    { path: 'cadastro', component: RegisterProfileComponent, canActivate: [AuthGuard] },
    { path: 'meu-portfolio', component: MyPortfolioComponent, canActivate: [AuthGuard] },
    { path: 'meus-alunos', component: ProfessorPortfolioComponent, canActivate: [AuthGuard] },
    { path: 'explorar', component: ExploreComponent, canActivate: [OpenGuard] },
    { path: 'meu-perfil', component: ProfileComponent, canActivate: [AuthGuard] },
    { path: 'nova-publicacao', component: NewPublicationComponent, canActivate: [AuthGuard] },
    { path: ':cdUsuario/:titlePost', component: PublicationDetailComponent, canActivate: [OpenGuard]},
    { path: ':url', component: ExternalAccessComponent, canActivate: [OpenGuard]}]
    // FIXME: remove authentication on PublicationDetailComponent


@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forRoot(routes)]
})
export class AppRoutingModule { }
