
import { Action } from '@ngrx/store'

import { PROFESSOR_USER, STUDENT_USER } from '../constants'


export class ProfessorUser implements Action {
    readonly type = PROFESSOR_USER

    constructor(public payload: boolean) {}
}

export type ViewActions = ProfessorUser
