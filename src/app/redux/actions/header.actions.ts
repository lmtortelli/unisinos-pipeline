
import { Action } from '@ngrx/store'

import { ACTIVE_MENU } from '../constants'


export class HeaderProfessor implements Action {
    readonly type = ACTIVE_MENU

    constructor(public payload: number) {}
}

export type HeaderActions = HeaderProfessor
