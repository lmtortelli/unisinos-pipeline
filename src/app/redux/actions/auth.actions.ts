
import { Action } from '@ngrx/store'

import { FETCH_USER, FETCH_USER_SUCCESS, FETCH_USER_FAIL, SET_TOKEN, LOGOUT } from '../constants'
import { Usuario } from '../../models/usuario'

export class FetchUser implements Action {
    readonly type = FETCH_USER

    constructor() {}
}

export class FetchUserSuccess implements Action {
  readonly type = FETCH_USER_SUCCESS

  constructor(public payload: Usuario) {}
}

export class SetToken implements Action {
  readonly type = SET_TOKEN

  constructor(public payload: string) {}
}
export class Logout implements Action {
  readonly type = LOGOUT

  constructor(public payload: string) {}
}

export class FetchUserFail implements Action {
  readonly type = FETCH_USER_FAIL

  constructor(public error: any) {}
}

export const fetchUser = async (store, userService) => {
  try {
    store.dispatch({ type: FETCH_USER })
    const user = await userService.getLoggedUser()
    return store.dispatch({ type: FETCH_USER_SUCCESS, payload: new Usuario(user) })
  } catch (err) {
    console.error(err)
    store.dispatch({ type: FETCH_USER_FAIL, error: err  })
  }
}

export const fetchUserOpenGuard = async (store, userService) => {
  try {
    store.dispatch({ type: FETCH_USER })
    const user = await userService.getLoggedUser()
    return store.dispatch({ type: FETCH_USER_SUCCESS, payload: new Usuario(user) })
  } catch (err) {
    store.dispatch({ type: FETCH_USER_FAIL, error: err  })
  }
}


export type AuthActions = FetchUser
  | FetchUserSuccess
  | FetchUserFail
  | SetToken
  | Logout
