import { ACTIVE_MENU} from '../constants'
import { HeaderActions } from '../actions'

const initialState = {
  active: 1,
  error: null
}

const header = (state = initialState, action: HeaderActions) => {
  switch (action.type) {
    case ACTIVE_MENU:
      return {
        ...state,
        active : action.payload
      }
    default:
      return state
  }
}

export default header
