import { FETCH_USER, FETCH_USER_SUCCESS, FETCH_USER_FAIL, SET_TOKEN, LOGOUT } from '../constants'
import { AuthActions } from '../actions'

const initialState = {
  isLoading: false,
  user: null,
  error: null
}

const auth = (state = initialState, action: AuthActions) => {
  switch (action.type) {
    case FETCH_USER:
      return {
        ...state,
        isLoading: true
      }
    case LOGOUT:
      return {
        user : null,
        state : undefined,
        token: null,
        isLoading: false
      }
    case FETCH_USER_SUCCESS:
      return {
        ...state,
        isLoading: false,
        user: action.payload
      }
    case FETCH_USER_FAIL:
      return {
        ...state,
        isLoading: false,
        error: action.error
      }
    case SET_TOKEN:
      return {
        ...state,
        token: action.payload
      }
    default:
      return state
  }
}

export default auth
