export { default as auth } from './auth.reducers'
export { default as view } from './view.reducers'
export { default as header } from './header.reducers'
