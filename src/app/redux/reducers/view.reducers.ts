import { PROFESSOR_USER, STUDENT_USER } from '../constants'
import { ViewActions } from '../actions'

const initialState = {
  isProfessor: true,
  error: null
}

const view = (state = initialState, action: ViewActions) => {
  if (action.payload) {
    if (action.payload['usuario']) {
      if (action.payload['usuario'].flagProfessor === 1) {
        state.isProfessor = true
      } else {
        state.isProfessor = false
      }
    }
  }
  switch (action.type) {
    case PROFESSOR_USER:
      return {
        ...state,
        isProfessor: state.isProfessor
      }
    default:
      return state
  }
}

export default view
