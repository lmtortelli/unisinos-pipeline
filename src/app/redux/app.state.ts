import { Usuario } from '../models/usuario'

export interface AppState {
    readonly auth: {
      isLoading: boolean,
      user: Usuario,
      token: string,
      error: any
    }

    readonly header: {
      active: number
    }

    readonly view: {
      isProfessor: boolean
    }


}
